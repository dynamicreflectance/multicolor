#pragma once

/* BufferUpdateMode.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/


namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    enum class BufferUpdateMode
    {
        _UNKNOWN = -1,
        STATIC,
        DYNAMIC,
        STREAM,
        _COUNT
    };

}
}
}