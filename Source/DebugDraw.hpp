#pragma once

/* DebugDraw.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "HardwareGeometryBuffer.hpp"
#include "PerspectiveCameraTransformation.hpp"

#include <AABB3.hpp>
#include <Color4.hpp>
#include <InOut.hpp>

namespace dynamicreflectance {
namespace multicolor
{
   
    struct DebugDrawPoint
    {
        common::math::Vector3 position;
        common::Color4f color;
        float size;

        DebugDrawPoint()
            : size(0.0f)
        {}

        DebugDrawPoint(const common::math::Vector3 &position, float size, const common::Color4f &color)
            : position(position)
            , size(size)
            , color(color)
        {}
    };

    struct DebugDrawBox
    {
        common::math::Vector3 center;
        common::math::Vector2 halfSize;
        common::Color4f color;

        DebugDrawBox()
        {}

        DebugDrawBox(const common::math::Vector3 &center, const common::math::Vector2 &halfSize, const common::Color4f &color)
            : center(center)
            , halfSize(halfSize)
            , color(color)
        {}
    };

    struct DebugDrawLine
    {
        common::math::Vector3 p0;
        common::math::Vector3 p1;
        common::Color4f color;
        float width;

        DebugDrawLine()
            : width(0.0f)
        {}

        DebugDrawLine(const common::math::Vector3 p0, const common::math::Vector3 p1, float width, const common::Color4f &color)
            : p0(p0)
            , p1(p1)
            , width(width)
            , color(color)
        {}
    };

    class DebugDraw
    {
    public:

        DebugDraw();
        ~DebugDraw();
    
        bool isCreated() const
        {
            return this->created;
        }
        
        void add(const std::vector<DebugDrawPoint> &points)
        {
            this->points.insert(this->points.end(), points.begin(), points.end());
        }

        void add(const std::vector<DebugDrawBox> &boxes)
        {
            this->boxes.insert(this->boxes.end(), boxes.begin(), boxes.end());
        }

        void add(const std::vector<DebugDrawLine> &lines)
        {
            this->lines.insert(this->lines.begin(), lines.begin(), lines.end());
        }

    private:

        std::array<common::math::Vector4, 4> getCalculateVertices(const DebugDrawBox &box);
        void drawAllBuffers(common::InOut<PerspectiveCameraTransformation> camera);

        void clearAllBuffers()
        {
            this->points.clear();
            this->lines.clear();
            this->boxes.clear();
        }
        
    private:

        friend class Renderer;
    
        HardwareGeometryBuffer pointGeometryData;
        HardwareGeometryBuffer lineGeometryBuffer;
        HardwareGeometryBuffer boxGeometryData;
        HardwareGeometryBuffer cubeGeometryData;
    
        graphicsdriver::GLResource program;
    
        graphicsdriver::GLResource projectionViewUniform;
        graphicsdriver::GLResource modelUniform;
        graphicsdriver::GLResource colorUniform;

        std::vector<DebugDrawPoint> points;
        std::vector<DebugDrawLine> lines;
        std::vector<DebugDrawBox> boxes;
        
        bool created;
    };

}
}