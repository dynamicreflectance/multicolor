#pragma once

/* BHVTree.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/


#include <AABB3.hpp>
#include <Vector3.hpp>
#include <Assert.hpp>
#include <InOut.hpp>

#include <vector>

namespace dynamicreflectance {
namespace common {
namespace math
{
    struct Frustum;
}
}
}

namespace dynamicreflectance {
namespace multicolor
{

    class Renderable;

    class BHVTree
    {
    private:

        static const int32_t NULL_NODE = -1;
        static const int32_t NULL_DATA = -1;

        struct BHVNode
        {
            int32_t parentId;
            int32_t leftChildId;
            int32_t rightChildId;

            int32_t dataId;

            common::math::AABB3 aabb;

            BHVNode()
                : parentId(NULL_NODE)
                , leftChildId(NULL_NODE)
                , rightChildId(NULL_NODE)
                , dataId(NULL_DATA)
            {}

            bool hasChildren() const
            {
                return NULL_NODE != this->leftChildId && NULL_NODE != this->rightChildId;
            }

            bool hasParent() const
            {
                return NULL_NODE != this->parentId;
            }

            bool hasData() const
            {
                return NULL_DATA != this->dataId;
            }
        };

    public:

        BHVTree(float aabbNodeFatFactor)
            : nodes{ BHVNode() }
            , rootId(0)
            , aabbNodeFatFactor(aabbNodeFatFactor)
        {}

        ~BHVTree()
        {}

        int32_t addLeaf(Renderable *pRenderable);

        int32_t getCalculateAllNodesCount()
        {
            int32_t nodesCount = (int32_t)this->nodes.size();
            int32_t removedNodesCount = (int32_t)this->nodesFreeList.size();

            return nodesCount - removedNodesCount;
        }

        int32_t getCalculateAllObjectsCount() const
        {
            int32_t l = (int32_t)this->data.size() - (int32_t)this->dataFreeList.size();
            return  l >= 0 ? l : 0;
        }

        void removeLeaf(int32_t id);
        void moveLeaf(int32_t id, const common::math::AABB3 &newAabb);

        std::vector<common::math::AABB3> getCalculateNodes(const common::math::Frustum &frustum) const;
        std::vector<const Renderable*> getCalculateLeaves(const common::math::Frustum &frustum) const;

    private:

        int32_t addLeaf(int32_t parentId, Renderable *pRenderable);
        int32_t allocateNode();
        int32_t allocateData(Renderable *pRenderable);
        void updateBHVNodeParent(int32_t parentId);
        void findRoot();

    private:

        std::vector<BHVNode> nodes;
        std::vector<Renderable*> data;

        std::vector<int32_t> nodesFreeList;
        std::vector<int32_t> dataFreeList;

        mutable std::vector<int32_t> traverseBuffer;

        int32_t rootId;
        float aabbNodeFatFactor;
    };

}
}