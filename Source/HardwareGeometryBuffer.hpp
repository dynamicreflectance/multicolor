#pragma once

/* HardwareGeometryBuffer.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "OpenGLDriverTypes.hpp"
#include "VertexAttribute.hpp"
#include "BufferUpdateMode.hpp"

#include <ReferenceCounted.hpp>

#include <vector>

namespace dynamicreflectance {
namespace multicolor
{

    class HardwareGeometryBuffer : public common::ReferenceCounted<HardwareGeometryBuffer>
    {
    public:
        HardwareGeometryBuffer()
            : vbo(-1)
            , ibo(-1)
            , vao(-1)
            , created(false)
        {}

        HardwareGeometryBuffer(const uint8_t *pVertices, int32_t verticesSizeInBytes, const uint8_t *pIndices, int32_t indicesSizeInBytes, const std::vector<graphicsdriver::VertexAttribute> &attributes, graphicsdriver::BufferUpdateMode updateMode = graphicsdriver::BufferUpdateMode::STATIC);
        ~HardwareGeometryBuffer();

        bool set(const uint8_t *pVertices, int32_t verticesSizeInBytes, const uint8_t *pIndices, int32_t indicesSizeInBytes, const std::vector<graphicsdriver::VertexAttribute> &attributes, graphicsdriver::BufferUpdateMode updateMode = graphicsdriver::BufferUpdateMode::STATIC);
        bool setVbo(const uint8_t *pVertices, int32_t verticesSizeInBytes, graphicsdriver::BufferUpdateMode updateMode = graphicsdriver::BufferUpdateMode::STATIC);
        bool setIbo(const uint8_t *pIndices, int32_t indicesSizeInBytes, graphicsdriver::BufferUpdateMode updateMode = graphicsdriver::BufferUpdateMode::STATIC);

        bool allocate(int32_t vboBufferSizeInBytes, int32_t iboBufferSizeInBytes, const std::vector<graphicsdriver::VertexAttribute> &attributes, graphicsdriver::BufferUpdateMode updateMode = graphicsdriver::BufferUpdateMode::STATIC)
        {
            return this->set(nullptr, vboBufferSizeInBytes, nullptr, iboBufferSizeInBytes, attributes, updateMode);
        }

        graphicsdriver::GLResource getVbo() const
        {
            return this->vbo;
        }

        graphicsdriver::GLResource getIbo() const
        {
            return this->ibo;
        }

        graphicsdriver::GLResource getVao() const
        {
            return this->vao;
        }

        bool isCreated() const
        {
            return this->created;
        }

    private:

        graphicsdriver::GLResource vbo;
        graphicsdriver::GLResource ibo;
        graphicsdriver::GLResource vao;

        bool created;
    };

}
}