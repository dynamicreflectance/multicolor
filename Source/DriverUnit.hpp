#pragma once

/* DriverUnit.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Context.hpp"
#include "glew/glew.h"


namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{


    class DriverUnit
    {
    public:

        DriverUnit(Context *pContext)
            : pContext(pContext)
        {}

    protected:

        Context *pContext;
    };

}
}
}