#pragma once

/* GeometryBufferUnit.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license.  See the LICENSE file for details.
*/

#include "OpenGLDriverTypes.hpp"
#include "VertexAttribute.hpp"
#include "DriverUnit.hpp"
#include "BufferUpdateMode.hpp"

#include <InOut.hpp>
#include <EntryLocation.hpp>
#include <Result.hpp>

#include <vector>


namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{

    enum class BufferType
    {
        _UNKNOWN = -1,
        VBO,
        IBO,
        _COUNT
    };

    class GeometryBufferUnit : public DriverUnit
    {
    public:

        GeometryBufferUnit(Context *pContext)
            : DriverUnit(pContext)
        {}

        GLResource create(BufferType type, BufferUpdateMode mode, const uint8_t *pData, int32_t dataLengthInBytes);
        GLResource create(BufferType type, BufferUpdateMode mode, int32_t dataLengthInBytes);
        GLResource create(GLResource vbo, GLResource ibo, const std::vector<VertexAttribute> &attributes);

        void setData(BufferType type, BufferUpdateMode mode, GLResource buffer, const uint8_t *pData, int32_t dataLengthInBytes);

        void release(common::InOut<GLResource> buffer);
  
    private:

        GLenum toGLType(VertexAttributeType type) const;
        GLenum toGLType(BufferUpdateMode mode) const;
        GLenum toGLType(BufferType type) const;
    };

}
}
}