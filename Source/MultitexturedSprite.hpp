#pragma once

/* MultitexturedSprite.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Renderable.hpp"
#include "Texture.hpp"
#include "Settings.hpp"

#include <Assert.hpp>

#include <vector>

namespace dynamicreflectance {
namespace multicolor
{
    class MultitexturedSprite : public Renderable
    {
    public:

        MultitexturedSprite(const std::vector<Texture> &textures, common::Color4f color, const ModelTransformation &transformation, float depth, BHVTreeTransformationCallback *pCallback)
            : Renderable(RenderableType::MULTITEXTURED_SPRITE, color, transformation, depth, pCallback)
            , textures(textures)
        {
            ASSERT(settings::MULTITEXTURE_TEXTURES_COUNT >= textures.size(), "Too many textures passed to sprite.");
        }

        const std::vector<Texture>& getTextures() const
        {
            return this->textures;
        }

    private:

        friend class Scene;

        std::vector<Texture> textures;
    };

}
}