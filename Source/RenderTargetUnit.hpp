#pragma once

/* RenderTargetUnit.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license.  See the LICENSE file for details.
*/


#include "DriverUnit.hpp"
#include "OpenGLDriverTypes.hpp"
#include "TextureFilterType.hpp"

#include <Result.hpp>
#include <InOut.hpp>
#include <ColorFormat.hpp>

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{

    enum class RenderTargetAttachmentFormat
    {
        _UNKNOWN = -1,
        RGBA4,
        RGBA8,
        RGB565,
        RGB5_A1,
        DEPTH_COMPONENT_16,
        DEPTH_COMPONENT_32,
        STENCIL_INDEX_8,
        _COUNT

    };

    enum class RenderTargetAttachmentType
    {
        _UNKNOWN = -1,
        DEPTH_ATTACHMENT,
        COLOR_ATTACHMENT_0,
        COLOR_ATTACHMENT_1,
        COLOR_ATTACHMENT_2,
        COLOR_ATTACHMENT_3,
        COLOR_ATTACHMENT_4,
        COLOR_ATTACHMENT_5,
        COLOR_ATTACHMENT_6,
        COLOR_ATTACHMENT_7,
        COLOR_ATTACHMENT_8,
        COLOR_ATTACHMENT_9,
        COLOR_ATTACHMENT_10,
        COLOR_ATTACHMENT_11,
        COLOR_ATTACHMENT_12,
        COLOR_ATTACHMENT_13,
        COLOR_ATTACHMENT_14,
        COLOR_ATTACHMENT_15,
        _COUNT
    };

    enum class RenderTargetStatus
    {
        _UNKNOWN = -1,
        OK,
        UNDEFINED,
        INCOMPLETE_ATTACHMENT,
        MISSING_ATTACHMENT,
        INCOMPLETE_DRAW_BUFFER,
        INCOMPLETE_READ_BUFFER,
        UNSUPPORTED,
        INCOMPLETE_MULTISAMPLE,
        INCOMPLETE_LAYER_TARGETS,
        _COUNT
    };

    enum class RenderTargetMode
    {
        _UNKNOWN = -1,
        DRAW,
        READ,
        DRAW_READ,
        _COUNT
    };

    class RenderTargetUnit : public DriverUnit
    {
    public:

        RenderTargetUnit(Context *pContext)
             : DriverUnit(pContext)
         {}

         GLResource create();
         GLResource attach(GLResource fbo, RenderTargetAttachmentType type, RenderTargetAttachmentFormat format, int32_t width, int32_t height);
         void attach(GLResource fbo, RenderTargetAttachmentType type, GLResource texture);

         void release(common::InOut<GLResource> fbo);
         void releaseAttachment(common::InOut<GLResource> attachment);

         void begin(GLResource fbo, RenderTargetMode mode);
         void end(RenderTargetMode mode);

         RenderTargetStatus checkStatus(GLResource fbo, RenderTargetMode mode);

    private:

        GLenum toGLType(RenderTargetAttachmentFormat type);
        GLenum toGLType(RenderTargetAttachmentType type);
        GLenum toGLType(RenderTargetMode);

        RenderTargetStatus toDriverUnitType(GLenum type);

    };

}
}
}