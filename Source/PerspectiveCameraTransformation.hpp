#pragma once

/* PerspectiveCameraTransformation.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "CameraTransformation.hpp"

#include <cmath>

namespace dynamicreflectance {
namespace multicolor
{
    class PerspectiveCameraTransformation : public CameraTransformation
    {
    public:

        PerspectiveCameraTransformation()
            : CameraTransformation()
        {}

        PerspectiveCameraTransformation(const PerspectiveCameraTransformation &rhs)
            : CameraTransformation(rhs)
        {}

        PerspectiveCameraTransformation(common::math::Vector3 position, float angleInRadians, float halfFovyInRadians, float aspectRatio, common::math::Vector2 farNear)
            : CameraTransformation(
                position, 
                angleInRadians, 
                common::math::Vector2(
                    (farNear.y * std::tan(halfFovyInRadians)),
                    -(farNear.y * std::tan(halfFovyInRadians))
                ), 
                common::math::Vector2(
                    (farNear.y * std::tan(halfFovyInRadians)) * aspectRatio,
                    -(farNear.y * std::tan(halfFovyInRadians )) * aspectRatio
                ),
                farNear
           )
        {
            this->calculateProjectionViewMatrix();
            this->calculateFrustum();
        }

        const common::math::Matrix4x4& getCalculateProjectionViewMatrix()
        {
            this->calculateProjectionViewMatrix();
            return this->projectionViewMatrix;
        }

        const common::math::Frustum& getCalculateFrustum()
        {
            this->calculateProjectionViewMatrix();
            this->calculateFrustum();

            return this->frustum;
        }

    private:

        void calculateProjectionViewMatrix() 
        {
            if (1 == this->status.projectionChanged)
            {
                this->projectionMatrix.setZero();

                this->projectionMatrix.rows[0].linear[0] = (2.0f * this->projectionFarNear.y) / (this->projectionLeftRight.y - this->projectionLeftRight.x);
                this->projectionMatrix.rows[1].linear[1] = (2.0f * this->projectionFarNear.y) / (this->projectionTopBottom.x - this->projectionTopBottom.y);
                this->projectionMatrix.rows[2].linear[0] = (this->projectionLeftRight.y + this->projectionLeftRight.x) / (this->projectionLeftRight.y - this->projectionLeftRight.x);
                this->projectionMatrix.rows[2].linear[1] = (this->projectionTopBottom.x + this->projectionTopBottom.y) / (this->projectionTopBottom.x - this->projectionTopBottom.y);
                this->projectionMatrix.rows[2].linear[3] = -1.0f;
                this->projectionMatrix.rows[2].linear[2] = -(this->projectionFarNear.x + this->projectionFarNear.y) / (this->projectionFarNear.x - this->projectionFarNear.y);
                this->projectionMatrix.rows[3].linear[2] = -(2.0f *this->projectionFarNear.x * this->projectionFarNear.y) / (this->projectionFarNear.x - this->projectionFarNear.y);
            }
          
            if (1 == this->status.viewChanged)
            {
                common::math::Vector3 f = normalize(common::math::Vector3(this->position.x, this->position.y, this->position.z + 1) - this->position.xyz);
                common::math::Vector3 s = normalize(cross(f, (common::math::Vector3(0.0f, 1.0f, 0.0f))));
                common::math::Vector3 u = cross(s, f);

                this->viewMatrix.setIdentity();

                this->viewMatrix.rows[0].linear[0] = s.x;
                this->viewMatrix.rows[1].linear[0] = s.y;
                this->viewMatrix.rows[2].linear[0] = s.z;
                this->viewMatrix.rows[0].linear[1] = u.x;
                this->viewMatrix.rows[1].linear[1] = u.y;
                this->viewMatrix.rows[2].linear[1] = u.z;
                this->viewMatrix.rows[0].linear[2] = -f.x;
                this->viewMatrix.rows[1].linear[2] = -f.y;
                this->viewMatrix.rows[2].linear[2] = -f.z;
                this->viewMatrix.rows[3].linear[0] = -dot(s, this->position.xyz);
                this->viewMatrix.rows[3].linear[1] = -dot(u, this->position.xyz);
                this->viewMatrix.rows[3].linear[2] = dot(f, this->position.xyz);
            }

            if (1 == this->status.projectionChanged || 1 == this->status.viewChanged)
            {
                this->projectionViewMatrix = this->projectionMatrix * this->viewMatrix;
            }

            this->status.projectionChanged = 0;
            this->status.viewChanged = 0;
        }

        void calculateFrustum()
        {
            if (1 == this->status.frustumChanged)
            {
                this->frustum.near.set(this->projectionViewMatrix.row0.linear[3] + this->projectionViewMatrix.row0.linear[2],
                                       this->projectionViewMatrix.row1.linear[3] + this->projectionViewMatrix.row1.linear[2],
                                       this->projectionViewMatrix.row2.linear[3] + this->projectionViewMatrix.row2.linear[2],
                                       this->projectionViewMatrix.row3.linear[3] + this->projectionViewMatrix.row3.linear[2]
                );
                
                this->frustum.far.set(this->projectionViewMatrix.row0.linear[3] - this->projectionViewMatrix.row0.linear[2],
                                      this->projectionViewMatrix.row1.linear[3] - this->projectionViewMatrix.row1.linear[2],
                                      this->projectionViewMatrix.row2.linear[3] - this->projectionViewMatrix.row2.linear[2],
                                      this->projectionViewMatrix.row3.linear[3] - this->projectionViewMatrix.row3.linear[2]
                );
                
                this->frustum.left.set(this->projectionViewMatrix.row0.linear[3] + this->projectionViewMatrix.row0.linear[0],
                                       this->projectionViewMatrix.row1.linear[3] + this->projectionViewMatrix.row1.linear[0],
                                       this->projectionViewMatrix.row2.linear[3] + this->projectionViewMatrix.row2.linear[0],
                                       this->projectionViewMatrix.row3.linear[3] + this->projectionViewMatrix.row3.linear[0]
                );
                
                this->frustum.right.set(this->projectionViewMatrix.row0.linear[3] - this->projectionViewMatrix.row0.linear[0],
                                        this->projectionViewMatrix.row1.linear[3] - this->projectionViewMatrix.row1.linear[0],
                                        this->projectionViewMatrix.row2.linear[3] - this->projectionViewMatrix.row2.linear[0],
                                        this->projectionViewMatrix.row3.linear[3] - this->projectionViewMatrix.row3.linear[0]
                );
                
                this->frustum.top.set(this->projectionViewMatrix.row0.linear[3] - this->projectionViewMatrix.row0.linear[1],
                                      this->projectionViewMatrix.row1.linear[3] - this->projectionViewMatrix.row1.linear[1],
                                      this->projectionViewMatrix.row2.linear[3] - this->projectionViewMatrix.row2.linear[1],
                                      this->projectionViewMatrix.row3.linear[3] - this->projectionViewMatrix.row3.linear[1]
                );
                
                this->frustum.bottom.set(this->projectionViewMatrix.row0.linear[3] + this->projectionViewMatrix.row0.linear[1],
                                         this->projectionViewMatrix.row1.linear[3] + this->projectionViewMatrix.row1.linear[1],
                                         this->projectionViewMatrix.row2.linear[3] + this->projectionViewMatrix.row2.linear[1],
                                         this->projectionViewMatrix.row3.linear[3] + this->projectionViewMatrix.row3.linear[1]
                );

                this->status.frustumChanged = 0;
            }
        }            
    };
}
}
