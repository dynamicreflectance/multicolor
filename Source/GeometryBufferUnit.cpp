/* GeometryBufferUnit.cpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license.  See the LICENSE file for details.
*/

#include "GeometryBufferUnit.hpp"

#include <Assert.hpp>

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    using namespace common;

    GLResource GeometryBufferUnit::create(BufferType type, BufferUpdateMode mode, const uint8_t *pData, int32_t dataLengthInBytes)
    {
        GLResource ret = 0;

        glGenBuffers(1, &ret);
        this->setData(type, mode, ret, pData, dataLengthInBytes);

        return ret;
    }

    GLResource GeometryBufferUnit::create(BufferType type, BufferUpdateMode mode, int32_t dataLengthInBytes)
    {
        GLResource ret = 0;

        glGenBuffers(1, &ret);
        this->setData(type, mode, ret, nullptr, dataLengthInBytes);

        return ret;
    }

    GLResource GeometryBufferUnit::create(GLResource vbo, GLResource ibo, const std::vector<VertexAttribute> &attributes)
    {
        GLResource ret = 0;

        glGenVertexArrays(1, &ret);
        glBindVertexArray(ret);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

        for (size_t i = 0; i < attributes.size(); i++)
        {
            glEnableVertexAttribArray((GLuint)i);
#ifdef _M_AMD64
            glVertexAttribPointer((GLuint)i, attributes[i].componentsCount, this->toGLType(attributes[i].type), GL_TRUE, attributes[i].vertexSizeInBytes, (void*)(int64_t*)(int64_t)(attributes[i].offsetInBytes));
#else
            glVertexAttribPointer((GLuint)i, attributes[i].componentsCount, this->toGLType(attributes[i].type), GL_FALSE, attributes[i].vertexSizeInBytes, (void*)(attributes[i].offsetInBytes));
#endif
        }

        glBindVertexArray(0);

        return ret;
    }

    void GeometryBufferUnit::setData(BufferType type, BufferUpdateMode mode, GLResource buffer, const uint8_t *pData, int32_t dataLengthInBytes)
    {
        glBindBuffer(this->toGLType(type), buffer);
        glBufferData(this->toGLType(type), dataLengthInBytes, pData, this->toGLType(mode));
        glBindBuffer(this->toGLType(type), 0);
    }

    void GeometryBufferUnit::release(InOut<GLResource> buffer)
    {
        glDeleteBuffers(1, buffer.getPointer()); buffer.getValue() = 0;
    }

    GLenum GeometryBufferUnit::toGLType(VertexAttributeType type) const
    {
        GLenum ret = 0;

        switch (type)
        {
            case VertexAttributeType::INT16:
            {
                ret = GL_SHORT;
            }
            break;

            case VertexAttributeType::UNSIGNED_INT16:
            {
                ret = GL_UNSIGNED_SHORT;
            }
            break;

            case VertexAttributeType::INT32:
            {
                ret = GL_INT;
            }
            break;

            case VertexAttributeType::UNSIGNED_INT32:
            {
                ret = GL_UNSIGNED_INT;
            }
            break;

            case VertexAttributeType::FLOAT:
            {
                ret = GL_FLOAT;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented vertex attribute type");
        }

        return ret;
    }

    GLenum GeometryBufferUnit::toGLType(BufferUpdateMode mode) const
    {
        GLenum ret = 0;

        switch (mode)
        {
            case BufferUpdateMode::STATIC:
            {
                ret = GL_STATIC_DRAW;
            }
            break;

            case BufferUpdateMode::DYNAMIC:
            {
                ret = GL_DYNAMIC_DRAW;
            }
            break;

            case BufferUpdateMode::STREAM:
            {
                ret = GL_STREAM_DRAW;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented buffer update mode");
        }

        return ret;
    }

    GLenum GeometryBufferUnit::toGLType(BufferType type) const
    {
        GLenum ret = 0;

        switch (type)
        {
            case BufferType::IBO:
            {
                ret = GL_ELEMENT_ARRAY_BUFFER;
            }
            break;

            case BufferType::VBO:
            {
                ret = GL_ARRAY_BUFFER;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented buffer type");
        }

        return ret;
    }
}
}
}