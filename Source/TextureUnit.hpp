/* TextureUnit.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#pragma once

#include "DriverUnit.hpp"
#include "OpenGLDriverTypes.hpp"
#include "TextureFilterType.hpp"
#include "TextureWrappingMode.hpp"

#include <ColorFormat.hpp>
#include <InOut.hpp>
#include <EntryLocation.hpp>
#include <Result.hpp>


namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    class TextureUnit : public DriverUnit
    {
    public:

        TextureUnit(Context *pContext)
            : DriverUnit(pContext)
        {}

        GLResource create(const uint8_t *pData, common::ColorFormat colorFormat, int32_t width, int32_t height, TextureFilterType magFilter, TextureFilterType minFilter, TextureWrappingMode wrapMode);
        
        void setActive(GLResource texture, int32_t id);
        void setFilters(GLResource texture, TextureFilterType magFilter, TextureFilterType minFilter);
        void setWrappingMode(GLResource texture, TextureWrappingMode wrappingMode);
        void setData(GLResource texture, const uint8_t *pData, common::ColorFormat colorFormat, int32_t width, int32_t height);
        void release(common::InOut<GLResource> texture);

    private:

        GLenum toGLType(TextureWrappingMode mode) const;
        GLenum toGLType(TextureFilterType filter) const;
        GLenum toGLType(common::ColorFormat format) const;
    };


}
}
}