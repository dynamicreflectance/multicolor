#pragma once

/* Texture.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "OpenGLDriver.hpp"
#include "TextureFilterType.hpp"
#include "TextureWrappingMode.hpp"

#include <ImageData.hpp>
#include <ReferenceCounted.hpp>
#include <Vector2.hpp>


namespace dynamicreflectance {
namespace multicolor
{
    class Texture : public common::ReferenceCounted<Texture>
    {
    public:

        Texture()
            : colorFormat(common::ColorFormat::RGB)
            , resolution(1, 1)
            , wrapMode(graphicsdriver::TextureWrappingMode::CLAMP_TO_EDGE)
            , magFilter(graphicsdriver::TextureFilterType::NEAREST)
            , minFilter(graphicsdriver::TextureFilterType::NEAREST)
            , transparent(false)
            , created(false)
        {
            //TODO: set to default texture unit
            uint8_t data[] = { 255, 255, 255 };

            graphicsdriver::OpenGLDriver::getInstance().clearErrors();

            this->texture = graphicsdriver::OpenGLDriver::getInstance().pTextureUnit->create(
                data,
                common::ColorFormat::RGB,
                1, 1,
                graphicsdriver::TextureFilterType::NEAREST,
                graphicsdriver::TextureFilterType::NEAREST,
                graphicsdriver::TextureWrappingMode::CLAMP_TO_EDGE
            );

            this->created = graphicsdriver::DriverError::NO_ERROR == graphicsdriver::OpenGLDriver::getInstance().getLastError();
        }

        Texture(const common::ImageData &data, graphicsdriver::TextureFilterType magFilter, graphicsdriver::TextureFilterType minFilter, graphicsdriver::TextureWrappingMode wrapMode, bool transparent)
            : colorFormat(data.format)
            , resolution(data.resolution)
            , wrapMode(wrapMode)
            , magFilter(magFilter)
            , minFilter(minFilter)
            , transparent(transparent)
        {
            graphicsdriver::OpenGLDriver::getInstance().clearErrors();
            this->texture = graphicsdriver::OpenGLDriver::getInstance().pTextureUnit->create(data.pData, data.format, data.resolution.width, data.resolution.height, magFilter, minFilter, wrapMode);
            this->created = graphicsdriver::DriverError::NO_ERROR == graphicsdriver::OpenGLDriver::getInstance().getLastError();
        }

        ~Texture()
        {
            this->decRef();

            if (true == this->isReleased())
            {
                graphicsdriver::OpenGLDriver::getInstance().pTextureUnit->release(common::inout(this->texture));
            }
        }

        graphicsdriver::TextureWrappingMode getWrappingMode() const
        {
            return this->wrapMode;
        }

        graphicsdriver::TextureFilterType getMagFilterType() const
        {
            return this->magFilter;
        }

        graphicsdriver::TextureFilterType getMinFilterType() const
        {
            return this->minFilter;
        }

        common::Resolution getResolution() const
        {
            return this->resolution;
        }

        bool isTransparent() const
        {
            return this->transparent;
        }

        bool isCreated() const
        {
            return this->created;
        }

        operator graphicsdriver::GLResource() const
        {
            return this->texture;
        }

        bool operator == (const Texture &rhs) const
        {
            return
                rhs.texture == this->texture &&
                rhs.colorFormat == this->colorFormat &&
                rhs.resolution == this->resolution &&
                rhs.magFilter == this->magFilter &&
                rhs.minFilter == this->minFilter &&
                rhs.wrapMode == this->wrapMode   
           ;
        }
    private:

        graphicsdriver::GLResource texture;

        common::ColorFormat colorFormat;
        common::Resolution resolution;

        graphicsdriver::TextureFilterType magFilter;
        graphicsdriver::TextureFilterType minFilter;
        graphicsdriver::TextureWrappingMode wrapMode;

        bool transparent;
        bool created;
    };
}
}