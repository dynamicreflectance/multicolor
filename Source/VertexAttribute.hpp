#pragma once

/* VertexAttribute.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <cstdint>

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    enum class VertexAttributeType
    {
        _UNKNOWN = -1,
        INT32,
        UNSIGNED_INT32,
        INT16,
        UNSIGNED_INT16,
        FLOAT,
        _COUNT
    };

    struct VertexAttribute
    {
        int32_t componentsCount;
        int32_t offsetInBytes;
        int32_t vertexSizeInBytes;
        VertexAttributeType type;

        VertexAttribute()
            : componentsCount(0)
            , offsetInBytes(0)
            , vertexSizeInBytes(0)
            , type(VertexAttributeType::_UNKNOWN)
        {
        }

        VertexAttribute(int32_t componentsCount, int32_t offsetInBytes, int32_t vertexSizeInBytes, VertexAttributeType type)
            : componentsCount(componentsCount)
            , offsetInBytes(offsetInBytes)
            , vertexSizeInBytes(vertexSizeInBytes)
            , type(type)
        {
        }

        bool operator == (VertexAttribute rhs) const
        {
            return
                this->componentsCount == rhs.componentsCount &&
                this->offsetInBytes == rhs.offsetInBytes &&
                this->vertexSizeInBytes == rhs.vertexSizeInBytes &&
                this->type == rhs.type
                ;

        }
    };
}
}
}