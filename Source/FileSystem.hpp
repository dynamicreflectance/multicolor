#pragma once

/* FileSystem.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/


#include <Singleton.hpp>
#include <Result.hpp>

#include <string>
#include <vector>

namespace dynamicreflectance {
namespace multicolor
{

    class FileSystem : public common::Singleton<FileSystem>
    {
    private:

        struct Token
        {
            const char *pStart;
            int32_t length;
        };

    public:

        std::string getCurrentDir() const;
        common::Result<std::string, bool> findAbsolutePath(const char *relativePath, const char *currentDir);

    private:

        friend common::Singleton<FileSystem>;

    private:

        std::vector<Token> tokenize(const char *string);

        bool isFolderUpDir(const Token &token);
        std::string buildString(const std::vector<Token> &tokens);
    };

}
}