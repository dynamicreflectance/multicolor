#pragma once

/* Timer.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <chrono>

namespace dynamicreflectance {
namespace multicolor
{
    class Timer
    {
    public:

        Timer(bool autostart = false)
        {
            if (true == autostart)
            {
                this->reset();
            }
        }

        ~Timer()
        {
        }

        void reset()
        {
            this->startTime = std::chrono::steady_clock::now();
        }

        float getElapsedTimeInSeconds()
        {
            std::chrono::steady_clock::time_point endTime = std::chrono::steady_clock::now();
            float time = (float)std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
            this->startTime = endTime;
            return time * this->divider;
        }

        int64_t getElapsedTimeInMiliseconds()
        {
            std::chrono::steady_clock::time_point endTime = std::chrono::steady_clock::now();
            int64_t time = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
            this->startTime = endTime;
            return time;
        }

        int64_t getElapsedTimeInMicroseconds()
        {
            std::chrono::steady_clock::time_point endTime = std::chrono::steady_clock::now();
            int64_t time = std::chrono::duration_cast<std::chrono::microseconds>(endTime - startTime).count();
            this->startTime = endTime;
            return time;
        }

    private:

        std::chrono::steady_clock::time_point startTime;
        const float divider = 1.0f / 1000.0f;
    };

}
}

