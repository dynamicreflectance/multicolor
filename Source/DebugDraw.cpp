/* DebugDraw.cpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "DebugDraw.hpp"
#include "OpenGLDriver.hpp"
#include "CameraTransformation.hpp"
#include "ModelTransformation.hpp"
#include "ShaderProgramSource.hpp"
#include "Renderer.hpp"

namespace dynamicreflectance {
namespace multicolor
{
    using namespace graphicsdriver;
    using namespace common;
    using namespace math;

    namespace
    {
        ShaderProgramSource f_debugDrawShaderProgramSource =
        {
            std::string(
                "in vec4 attributePosition;"

                "uniform mat4 model;"
                "uniform mat4 projectionView;"

                "void main()"
                "{"
                "	gl_Position = projectionView * model * attributePosition;"
                "}"
                )
            ,
            std::string(
                "#version 330 core\n"

                "out vec4 fragmentColor;"
                "uniform vec4 color;"

                "void main()"
                "{"
                "	fragmentColor = color;"
                "}"
                )
            ,
            { "attributePosition" }
        };

    }

    DebugDraw::DebugDraw()
        : created(false)
    {
        GLResource vertexShader = OpenGLDriver::getInstance().pShaderUnit->createShader(ShaderType::VERTEX_SHADER, f_debugDrawShaderProgramSource.vertexShader);
        GLResource fragmentShader = OpenGLDriver::getInstance().pShaderUnit->createShader(ShaderType::FRAGMENT_SHADER, f_debugDrawShaderProgramSource.fragmentShader);

        this->created = true == OpenGLDriver::getInstance().pShaderUnit->isShaderCreated(vertexShader) && true == OpenGLDriver::getInstance().pShaderUnit->isShaderCreated(fragmentShader);

        if (true == this->created)
        {
            GLResource program = OpenGLDriver::getInstance().pShaderUnit->createShaderProgram(vertexShader, fragmentShader, f_debugDrawShaderProgramSource.attributes);
            this->created = OpenGLDriver::getInstance().pShaderUnit->isShaderProgramCreated(program);

            if (true == created)
            {
                this->created = true == OpenGLDriver::getInstance().pShaderUnit->isUniform(program, "color") &&
                                true == OpenGLDriver::getInstance().pShaderUnit->isUniform(program, "projectionView") &&
                                true == OpenGLDriver::getInstance().pShaderUnit->isUniform(program, "model")
                    ;

                if (true == this->created)
                {
                    uint16_t pointIboData[] =
                    {
                        0
                    };

                    uint16_t lineIboData[] =
                    {
                        0, 1
                    };

                    uint16_t boxIboData[] =
                    {
                        0, 1,
                        1, 2,
                        2, 3,
                        3, 0
                    };

                    uint16_t cubeIboData[] =
                    {
                        0, 1,
                        1, 2,
                        2, 3,
                        3, 0,

                        4, 5,
                        5, 6,
                        6, 7,
                        7, 4,

                        0, 4,
                        3, 7,

                        1, 5,
                        2, 6
                    };

                    this->colorUniform          = OpenGLDriver::getInstance().pShaderUnit->getUniformLocation(program, "color");
                    this->projectionViewUniform = OpenGLDriver::getInstance().pShaderUnit->getUniformLocation(program, "projectionView");
                    this->modelUniform          = OpenGLDriver::getInstance().pShaderUnit->getUniformLocation(program, "model");

                    this->program = program;

                    this->pointGeometryData.set((uint8_t*)&(Vector4(0.0f, 0.0f, 0.0f, 1.0f)), sizeof(Vector4), (uint8_t*)pointIboData, sizeof(pointIboData), { VertexAttribute(4, 0, sizeof(Vector4), VertexAttributeType::FLOAT) }, BufferUpdateMode::STATIC);
                    this->lineGeometryBuffer.set(nullptr, sizeof(Vector4) * 2, (uint8_t*)lineIboData, sizeof(int16_t) * 2, { VertexAttribute(4, 0, sizeof(Vector4), VertexAttributeType::FLOAT) }, BufferUpdateMode::STREAM);
                    this->boxGeometryData.set(nullptr, sizeof(Vector4) * 4, (uint8_t*)boxIboData, sizeof(boxIboData), { VertexAttribute(4, 0, sizeof(Vector4), VertexAttributeType::FLOAT) }, BufferUpdateMode::STREAM);
                    this->cubeGeometryData.set(nullptr, sizeof(Vector4) * 8, (uint8_t*)cubeIboData, sizeof(cubeIboData), { VertexAttribute(4, 0, sizeof(Vector4), VertexAttributeType::FLOAT) }, BufferUpdateMode::STREAM);
                }
                else
                {
                    OpenGLDriver::getInstance().pShaderUnit->releaseShaderProgram(inout(this->program));
                }
            }
        }

        if (true == OpenGLDriver::getInstance().pShaderUnit->isShaderCreated(vertexShader))
        {
            OpenGLDriver::getInstance().pShaderUnit->releaseShader(inout(vertexShader));
        }

        if (true == OpenGLDriver::getInstance().pShaderUnit->isShaderCreated(fragmentShader))
        {
            OpenGLDriver::getInstance().pShaderUnit->releaseShader(inout(fragmentShader));
        }
    }

    DebugDraw::~DebugDraw()
    {
        if (true == this->created)
        {
            OpenGLDriver::getInstance().pShaderUnit->releaseShaderProgram(inout(this->program));
        }
    }

    std::array<common::math::Vector4, 4> DebugDraw::getCalculateVertices(const DebugDrawBox &box)
    {
        std::array<common::math::Vector4, 4> ret =
        {
            common::math::Vector4(-box.halfSize.x, -box.halfSize.y, 0.0f, 1.0f),
            common::math::Vector4(-box.halfSize.x, box.halfSize.y, 0.0f, 1.0f),
            common::math::Vector4(box.halfSize.x, box.halfSize.y, 0.0f, 1.0f),
            common::math::Vector4(box.halfSize.x, -box.halfSize.y, 0.0f, 1.0f)
        };

        return ret;
    }

    void DebugDraw::drawAllBuffers(InOut<PerspectiveCameraTransformation> camera)
    {
        OpenGLDriver::getInstance().pShaderUnit->setUniformMatrix4x4(this->program, this->projectionViewUniform, camera.getPointer()->getCalculateProjectionViewMatrix());

        for (int32_t i = 0; i < (int32_t)this->points.size(); i++)
        {
            OpenGLDriver::getInstance().pShaderUnit->setUniformColor4f(this->program, this->colorUniform, this->points[i].color);
            OpenGLDriver::getInstance().setPointSize(this->points[i].size);

            OpenGLDriver::getInstance().pShaderUnit->setUniformMatrix4x4(this->program, this->modelUniform, ModelTransformation(0.0f, this->points[i].position, Vector2(1.0f, 1.0f)).getCalculateMatrix());
            OpenGLDriver::getInstance().draw(DrawingMode::POINTS, this->program, this->pointGeometryData.getVao(), 1);
        }

        for (int32_t i = 0; i < (int32_t)this->lines.size(); i++)
        {
            std::array<Vector4, 2> vertices = { Vector4(this->lines[i].p0, 1.0f), Vector4(this->lines[i].p1, 1.0f) };
            this->lineGeometryBuffer.setVbo((uint8_t*)vertices.data(), (int32_t)(sizeof(vertices[0]) * vertices.size()), BufferUpdateMode::STREAM);

            OpenGLDriver::getInstance().setLineWidth(this->lines[i].width);

            OpenGLDriver::getInstance().pShaderUnit->setUniformColor4f(this->program, this->colorUniform, this->lines[i].color);
            OpenGLDriver::getInstance().pShaderUnit->setUniformMatrix4x4(this->program, this->modelUniform, Matrix4x4(1.0f));

            OpenGLDriver::getInstance().draw(DrawingMode::LINES, this->program, this->lineGeometryBuffer.getVao(), 2);
        }

        for (int32_t i = 0; i < (int32_t)this->boxes.size(); i++)
        {
            std::array<Vector4, 4> vertices = this->getCalculateVertices(this->boxes[i]);
            this->boxGeometryData.setVbo((uint8_t*)vertices.data(), (int32_t)(sizeof(vertices[0]) * vertices.size()), BufferUpdateMode::STREAM);

            OpenGLDriver::getInstance().pShaderUnit->setUniformColor4f(this->program, this->colorUniform, this->boxes[i].color);
            OpenGLDriver::getInstance().pShaderUnit->setUniformMatrix4x4(this->program, this->modelUniform, ModelTransformation(0.0f, this->boxes[i].center, Vector2(1.0f, 1.0f)).getCalculateMatrix());

            OpenGLDriver::getInstance().draw(DrawingMode::TRIANGLE_FAN, this->program, this->boxGeometryData.getVao(), 6);
        }

        OpenGLDriver::getInstance().setPointSize(1.0f);
        OpenGLDriver::getInstance().setLineWidth(1.0f);

    }
}
}