#pragma once

/* Sprite.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Renderable.hpp"
#include "Texture.hpp"

#include <Vector2.hpp>
#include <Color4.hpp>

namespace dynamicreflectance {
namespace multicolor
{
    class Sprite : public Renderable
    {
    public:

        Sprite(const Texture &texture, common::Color4f color, const ModelTransformation &transformation, float depth, BHVTreeTransformationCallback *pCallback)
            : Renderable(RenderableType::SPRITE, color, transformation, depth, pCallback)
            , texture(texture)
        {}

        Texture getTexture() const
        {
            return this->texture;
        }
        
    private:

        Texture texture;
        
    };

}
}