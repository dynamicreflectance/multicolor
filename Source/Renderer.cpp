/* Renderer.cpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Renderer.hpp"
#include "Vertex.hpp"
#include "Face.hpp"
#include "Sprite.hpp"
#include "MultitexturedSprite.hpp"
#include "Scene.hpp"
#include "ShaderProgramSource.hpp"
#include "DebugDraw.hpp"

#include "OpenGLDriver.hpp"

#include <InOut.hpp>
#include <Assert.hpp>
#include <Math.hpp>
#include <Logger.hpp>

namespace dynamicreflectance {
namespace multicolor
{
    using namespace graphicsdriver;
    using namespace common;
    using namespace math;

    namespace
    {
        std::string glslVersionString = "#version 330 core\n";

        ShaderProgramSource f_uberShaderProgramSource =
        {
            std::string(
                "in vec4 attributePosition;"
                "in vec2 attributeTexcoord;"

                "out vec2 texcoord;"

                "uniform mat4 projectionView;"
                "uniform mat4 model;"

                "void main()"
                "{"
                "	texcoord = attributeTexcoord;"
                "	gl_Position = projectionView * model * attributePosition;"
                "}"
            )
            ,
            std::string(
                "in vec2 texcoord;"

                "out vec4 fragmentColor;"

                "uniform sampler2D samplers[MULTITEXTURE_TEXTURES_COUNT];"
                "uniform vec2 framePositions[MULTITEXTURE_TEXTURES_COUNT];"
                "uniform vec2 texcoordOffsets[MULTITEXTURE_TEXTURES_COUNT];"
                "uniform vec4 color;"

                "void main()"
                "{"
                ""
                "   vec4 blendedFragmentColor = vec4(1.0, 1.0, 1.0, 1.0);"
                "   for(int i = 0; i < MULTITEXTURE_TEXTURES_COUNT; i++)"
                "   {"
                "       vec2 finalTexcoord = vec2((framePositions[i].y + texcoord.x) * texcoordOffsets[i].x, (framePositions[i].x + texcoord.y) * texcoordOffsets[i].y);"
                "       blendedFragmentColor *= texture(samplers[i], finalTexcoord);"
                "   }"
                "	fragmentColor = blendedFragmentColor * color;"
                "}"
            )
            ,
            { "attributePosition", "attributeTexcoord" }
        };

        Vertex f_vertices[] =
        {
            Vertex{ Vector4(-0.5f, -0.5f, 0.0f, 1.0f), Vector2(0.0f, 0.0f) },
            Vertex{ Vector4(-0.5f, 0.5f, 0.0f, 1.0f), Vector2(0.0f, 1.0f) },
            Vertex{ Vector4(0.5f, 0.5f, 0.0f, 1.0f), Vector2(1.0f, 1.0f) },
            Vertex{ Vector4(0.5f, -0.5f, 0.0f, 1.0f), Vector2(1.0f, 0.0f) },
        };

        Face f_faces[] =
        {
            Face(0, 1, 2),
            Face(2, 3, 0)
        };

        std::vector<VertexAttribute> f_attributes =
        {
            VertexAttribute(4, 0, (int32_t)sizeof(Vertex), VertexAttributeType::FLOAT),
            VertexAttribute(2, (int32_t)offsetof(Vertex, texcoord), (int32_t)sizeof(Vertex), VertexAttributeType::FLOAT)
        };

    }

    bool compare(const Renderer::RenderCommand &a, const Renderer::RenderCommand &b)
    {
        return a.transformation.getPosition().z > b.transformation.getPosition().z;
    }


    Renderer::Renderer(Resolution resolution, DebugDraw *pDebugDraw, Logger *pLogger)
        : created(false)
        , resolution(resolution)
        , pLogger(pLogger)
        , pDebugDraw(pDebugDraw)
    {

        struct Status
        {
            bool shadersCreated : 1;
            bool shaderProgramLinked : 1;
            bool defaultTextureCreated : 1;
            bool renderTargetCreated : 1;

            uint32_t notUsed : 28;

            Status()
                : shadersCreated(false)
                , shaderProgramLinked(false)
                , defaultTextureCreated(false)
                , renderTargetCreated(false)
                , notUsed(0)
            {}
        };

        Status status;

        const std::string fragmentShaderDefines = "#define MULTITEXTURE_TEXTURES_COUNT " + std::to_string(settings::MULTITEXTURE_TEXTURES_COUNT) + "\n";

        GLResource vertexShader = OpenGLDriver::getInstance().pShaderUnit->createShader(ShaderType::VERTEX_SHADER, std::vector<std::string>{ glslVersionString, f_uberShaderProgramSource.vertexShader });
        GLResource fragmentShader = OpenGLDriver::getInstance().pShaderUnit->createShader(ShaderType::FRAGMENT_SHADER, std::vector<std::string>{ glslVersionString, fragmentShaderDefines, f_uberShaderProgramSource.fragmentShader });

        bool isVertexShaderCreated = OpenGLDriver::getInstance().pShaderUnit->isShaderCreated(vertexShader);
        bool isFragmentShaderCreated = OpenGLDriver::getInstance().pShaderUnit->isShaderCreated(fragmentShader);

        this->created = true == isVertexShaderCreated && true == isFragmentShaderCreated;

        if (true == this->created)
        {
            status.shadersCreated = true;

            this->program = OpenGLDriver::getInstance().pShaderUnit->createShaderProgram(vertexShader, fragmentShader, f_uberShaderProgramSource.attributes);
            this->created = OpenGLDriver::getInstance().pShaderUnit->isShaderProgramCreated(this->program);

            if (true == this->created)
            {
                status.shaderProgramLinked = true;

                this->created = true == OpenGLDriver::getInstance().pShaderUnit->isUniform(this->program, "model") &&
                    true == OpenGLDriver::getInstance().pShaderUnit->isUniform(this->program, "projectionView") &&
                    true == OpenGLDriver::getInstance().pShaderUnit->isUniform(this->program, "color")
                    ;

                for (int32_t i = 0; i < settings::MULTITEXTURE_TEXTURES_COUNT && true == this->created; i++)
                {
                    this->created = true == this->created && true == OpenGLDriver::getInstance().pShaderUnit->isUniform(this->program, "samplers[" + std::to_string(i) + "]");
                    this->created = true == this->created && true == OpenGLDriver::getInstance().pShaderUnit->isUniform(this->program, "framePositions[" + std::to_string(i) + "]");
                    this->created = true == this->created && true == OpenGLDriver::getInstance().pShaderUnit->isUniform(this->program, "texcoordOffsets[" + std::to_string(i) + "]");
                }

                if (true == this->created)
                {
                    this->uniforms.modelUniform = OpenGLDriver::getInstance().pShaderUnit->getUniformLocation(this->program, "model");
                    this->uniforms.projectionViewUniform = OpenGLDriver::getInstance().pShaderUnit->getUniformLocation(this->program, "projectionView");
                    this->uniforms.colorUniform = OpenGLDriver::getInstance().pShaderUnit->getUniformLocation(this->program, "color");

                    for (int32_t i = 0; i < settings::MULTITEXTURE_TEXTURES_COUNT; i++)
                    {
                        this->uniforms.textures[i].sampler = OpenGLDriver::getInstance().pShaderUnit->getUniformLocation(this->program, "samplers[" + std::to_string(i) + "]");
                        this->uniforms.textures[i].framePositionUniform = OpenGLDriver::getInstance().pShaderUnit->getUniformLocation(this->program, "framePositions[" + std::to_string(i) + "]");
                        this->uniforms.textures[i].texcoordOffsetUniform = OpenGLDriver::getInstance().pShaderUnit->getUniformLocation(this->program, "texcoordOffsets[" + std::to_string(i) + "]");
                    }

                    this->created = this->geometryBuffer.set((uint8_t*)f_vertices, sizeof(f_vertices), (uint8_t*)f_faces, sizeof(f_faces), f_attributes);

                    if (true == this->created)
                    {
                        OpenGLDriver::getInstance().clearErrors();

                        uint8_t defaultTextureData[] = { 255, 255, 255, 255 };
                        this->defaultTexture = OpenGLDriver::getInstance().pTextureUnit->create(
                            defaultTextureData,
                            ColorFormat::RGBA,
                            1, 1,
                            TextureFilterType::NEAREST, TextureFilterType::NEAREST,
                            TextureWrappingMode::CLAMP_TO_EDGE
                            );

                        this->created = DriverError::NO_ERROR == OpenGLDriver::getInstance().getLastError();

                        if (true == this->created)
                        {
                            status.defaultTextureCreated = true;

                            this->renderTarget.fbo = OpenGLDriver::getInstance().pRenderTargetUnit->create();
                            this->renderTarget.colorAttachment = OpenGLDriver::getInstance().pRenderTargetUnit->attach(this->renderTarget.fbo, RenderTargetAttachmentType::COLOR_ATTACHMENT_0, RenderTargetAttachmentFormat::RGBA8, this->resolution.width, this->resolution.height);
                            this->renderTarget.depthAttachment = OpenGLDriver::getInstance().pRenderTargetUnit->attach(this->renderTarget.fbo, RenderTargetAttachmentType::DEPTH_ATTACHMENT, RenderTargetAttachmentFormat::DEPTH_COMPONENT_16, this->resolution.width, this->resolution.height);

                            RenderTargetStatus rtStatus = OpenGLDriver::getInstance().pRenderTargetUnit->checkStatus(this->renderTarget.fbo, RenderTargetMode::DRAW_READ);

                            this->created = RenderTargetStatus::OK == rtStatus;

                            if (true == this->created)
                            {
                                status.renderTargetCreated = true;

                                OpenGLDriver::getInstance().enable(DriverCapability::DEPTH_TEST);
                            }
                        }
                    }
                }
            }
        } 

        if (false == this->created)
        {
            if (true == status.shadersCreated)
            {
                OpenGLDriver::getInstance().pShaderUnit->releaseShader(inout(vertexShader));
                OpenGLDriver::getInstance().pShaderUnit->releaseShader(inout(fragmentShader));
            }

            if (true == status.shaderProgramLinked)
            {
                OpenGLDriver::getInstance().pRenderTargetUnit->release(inout(this->renderTarget.fbo));
            }

            if (true == status.defaultTextureCreated)
            {
                OpenGLDriver::getInstance().pTextureUnit->release(inout(this->defaultTexture));
            }

            if (true == status.renderTargetCreated)
            {
                OpenGLDriver::getInstance().pRenderTargetUnit->release(inout(this->renderTarget.fbo));
                OpenGLDriver::getInstance().pRenderTargetUnit->releaseAttachment(inout(this->renderTarget.colorAttachment));
                OpenGLDriver::getInstance().pRenderTargetUnit->releaseAttachment(inout(this->renderTarget.depthAttachment));
            }
        }
    }

    Renderer::~Renderer()
    {
        if (true == this->created)
        {
            OpenGLDriver::getInstance().pTextureUnit->release(inout(this->defaultTexture));
            OpenGLDriver::getInstance().pRenderTargetUnit->release(inout(this->renderTarget.fbo));
            OpenGLDriver::getInstance().pRenderTargetUnit->releaseAttachment(inout(this->renderTarget.colorAttachment));
            OpenGLDriver::getInstance().pRenderTargetUnit->releaseAttachment(inout(this->renderTarget.depthAttachment));
        }

        OpenGLDriver::getInstance().pShaderUnit->releaseShader(inout(this->program));
    }

    void Renderer::draw(const std::vector<const Renderable*> &objects, InOut<PerspectiveCameraTransformation> camera)
    {
        this->fillBuffers(objects, out(this->opaqueObjects), out(this->transparentObjects));
        OpenGLDriver::getInstance().pShaderUnit->setUniformMatrix4x4(this->program, this->uniforms.projectionViewUniform, camera.getPointer()->getCalculateProjectionViewMatrix());

        for (int32_t i = 0; i < (int32_t)this->opaqueObjects.size(); i++)
        {
            for (int32_t t = 0; t < settings::MULTITEXTURE_TEXTURES_COUNT; t++)
            {
                OpenGLDriver::getInstance().pTextureUnit->setActive(this->opaqueObjects[i].textures[t].handle, (int32_t)t);
                OpenGLDriver::getInstance().pShaderUnit->setUniformi32(this->program, this->uniforms.textures[t].sampler, (int32_t)t);
                OpenGLDriver::getInstance().pShaderUnit->setUniformVector2(this->program, this->uniforms.textures[t].framePositionUniform, this->opaqueObjects[i].textures[t].framePosition);
                OpenGLDriver::getInstance().pShaderUnit->setUniformVector2(this->program, this->uniforms.textures[t].texcoordOffsetUniform, this->opaqueObjects[i].textures[t].frameOffset);
            }

            OpenGLDriver::getInstance().pShaderUnit->setUniformColor4f(this->program, this->uniforms.colorUniform, this->opaqueObjects[i].color);
            OpenGLDriver::getInstance().pShaderUnit->setUniformMatrix4x4(this->program, this->uniforms.modelUniform, this->opaqueObjects[i].transformation.getCalculateMatrix());
            
            OpenGLDriver::getInstance().draw(DrawingMode::TRIANGLES, this->program, this->geometryBuffer.getVao(), 6);
        }

        if (nullptr != this->pDebugDraw)
        {
            this->pDebugDraw->drawAllBuffers(inout(camera));
        }

        OpenGLDriver::getInstance().disable(DriverCapability::WRITE_TO_DEPTH_BUFFER);
        OpenGLDriver::getInstance().enable(DriverCapability::BLEND);
        OpenGLDriver::getInstance().setBlendingMode(BlendingFactor::SRC_ALPHA, BlendingFactor::ONE_MINUS_SRC_ALPHA);

        std::sort(this->transparentObjects.begin(), this->transparentObjects.end(), compare);

        for (int32_t i = 0; i < (int32_t)this->transparentObjects.size(); i++)
        {
            for (int32_t t = 0; t < settings::MULTITEXTURE_TEXTURES_COUNT; t++)
            {
                OpenGLDriver::getInstance().pTextureUnit->setActive(this->transparentObjects[i].textures[t].handle, (int32_t)t);
                OpenGLDriver::getInstance().pShaderUnit->setUniformi32(this->program, this->uniforms.textures[t].sampler, (int32_t)t);
                OpenGLDriver::getInstance().pShaderUnit->setUniformVector2(this->program, this->uniforms.textures[t].framePositionUniform, this->transparentObjects[i].textures[t].framePosition);
                OpenGLDriver::getInstance().pShaderUnit->setUniformVector2(this->program, this->uniforms.textures[t].texcoordOffsetUniform, this->transparentObjects[i].textures[t].frameOffset);
            }

            OpenGLDriver::getInstance().pShaderUnit->setUniformColor4f(this->program, this->uniforms.colorUniform, this->transparentObjects[i].color);
            OpenGLDriver::getInstance().pShaderUnit->setUniformMatrix4x4(this->program, this->uniforms.modelUniform, this->transparentObjects[i].transformation.getCalculateMatrix());

            OpenGLDriver::getInstance().draw(DrawingMode::TRIANGLES, this->program, this->geometryBuffer.getVao(), 6);
        }

        OpenGLDriver::getInstance().enable(DriverCapability::WRITE_TO_DEPTH_BUFFER);
        OpenGLDriver::getInstance().disable(DriverCapability::BLEND);
    }

    void Renderer::begin(const Color4f &clearColor)
    {
        OpenGLDriver::getInstance().clear(clearColor);
        OpenGLDriver::getInstance().pRenderTargetUnit->begin(this->renderTarget.fbo, RenderTargetMode::DRAW);
        OpenGLDriver::getInstance().clear(clearColor);
        OpenGLDriver::getInstance().setViewport(0, 0, this->resolution.width, this->resolution.height);
    }

    void Renderer::end()
    {
        OpenGLDriver::getInstance().pRenderTargetUnit->end(RenderTargetMode::DRAW);
        OpenGLDriver::getInstance().draw(this->renderTarget.fbo, { RenderTargetAttachmentType::COLOR_ATTACHMENT_0 }, this->resolution.width, this->resolution.height, true, false);

        if (nullptr != this->pDebugDraw)
        {
            this->pDebugDraw->clearAllBuffers();
        }
    }

    void Renderer::fillBuffers(const std::vector<const Renderable*> &objects, Out<std::vector<RenderCommand>> opaqueObjectsBuffer, Out<std::vector<RenderCommand>> transparentObjectsBuffer)
    {
        RenderCommand command;

        opaqueObjectsBuffer.getPointer()->clear();
        transparentObjectsBuffer.getPointer()->clear();

        for (int32_t i = 0; i < (int32_t)objects.size(); i++)
        {
            command.color = objects[i]->getColor();
            command.transformation = objects[i]->getTransformation();

            for (int32_t t = 0; t < settings::MULTITEXTURE_TEXTURES_COUNT; t++)
            {
                command.textures[t].handle = this->defaultTexture;
                command.textures[t].framePosition = { 0.0f, 0.0f };
                command.textures[t].frameOffset = { 1.0f, 1.0f };
            }

            switch (objects[i]->getType())
            {
                case RenderableType::SPRITE:
                {
                    command.textures[0].handle = ((Sprite*)objects[i])->getTexture();                    
                }
                break;

                case RenderableType::MULTITEXTURED_SPRITE:
                {
                    for (int32_t t = 0; t < (int32_t)((MultitexturedSprite*)objects[i])->getTextures().size() && t < settings::MULTITEXTURE_TEXTURES_COUNT; t++)
                    {
                        command.textures[t].handle = ((MultitexturedSprite*)objects[i])->getTextures()[t];
                    }
                }
                break;

                case RenderableType::SPRITESHEET:
                {
                    Spritesheet *pSpritesheet = ((Spritesheet*)objects[i]);
                    command.textures[0].handle = pSpritesheet->getTexture();
                    command.textures[0].framePosition = { (float)(pSpritesheet->getActualFrame() / pSpritesheet->getFrameArrayWidth()), (float)(pSpritesheet->getActualFrame() % pSpritesheet->getFrameArrayWidth()) };
                    command.textures[0].frameOffset = { 1.0f / pSpritesheet->getFrameArrayWidth(), 1.0f / pSpritesheet->getFrameArrayHeight() };
                }
                break;

                DEFAULT_CASE_ASSERT("Not implemented renderable type");
            }

            if (false == ((Sprite*)objects[i])->getTexture().isTransparent() && areScalarsEqual(1.0f, command.color.a))
            {
                opaqueObjectsBuffer.getPointer()->push_back(command);
            }
            else
            {
                transparentObjectsBuffer.getPointer()->push_back(command);
            }

        }
    }
}
}
