#pragma once

/* Context.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license.  See the LICENSE file for details.
*/

#include "OpenGLDriverTypes.hpp"

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    struct Context
    {
        GLResource program;
        GLResource vao;
    };
}
}
}