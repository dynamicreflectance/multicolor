#pragma once

/* Vertex.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <Vector4.hpp>
#include <Vector2.hpp>

namespace dynamicreflectance {
namespace multicolor
{

    struct Vertex
    {
        common::math::Vector4 position;
        common::math::Vector2 texcoord;
    };

}
}