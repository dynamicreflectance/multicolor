/* RenderTargetUnit.cpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license.  See the LICENSE file for details.
*/

#include "RenderTargetUnit.hpp"

#include <Assert.hpp>

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    using namespace common;

    GLResource RenderTargetUnit::create()
    {
        GLResource ret = 0;

        glGenFramebuffers(1, &ret);
        return ret;
    }

    GLResource RenderTargetUnit::attach(GLResource fbo, RenderTargetAttachmentType type, RenderTargetAttachmentFormat format, int32_t width, int32_t height)
    {
        GLResource ret = 0;

        glBindFramebuffer(GL_FRAMEBUFFER, fbo);

            glGenRenderbuffers(1, &ret);
            glBindRenderbuffer(GL_RENDERBUFFER, ret);
            glRenderbufferStorage(GL_RENDERBUFFER, this->toGLType(format), width, height);
            
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, this->toGLType(type), GL_RENDERBUFFER, ret);

            glBindRenderbuffer(GL_RENDERBUFFER, 0);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        return ret;
    }

    void RenderTargetUnit::attach(GLResource fbo, RenderTargetAttachmentType type, GLResource texture)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glFramebufferTexture2D(GL_FRAMEBUFFER, this->toGLType(type), GL_TEXTURE_2D, (GLuint)texture, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void RenderTargetUnit::release(InOut<GLResource> fbo)
    {
        glDeleteFramebuffers(1, fbo.getPointer());
        fbo.getValue() = 0;
    }

    void RenderTargetUnit::releaseAttachment(InOut<GLResource> attachment)
    {
        glDeleteRenderbuffers(1, attachment.getPointer());
        attachment.getValue() = 0;
    }

    void RenderTargetUnit::begin(GLResource fbo, RenderTargetMode mode)
    {
        glBindFramebuffer(this->toGLType(mode), fbo);
    }

    void RenderTargetUnit::end(RenderTargetMode mode)
    {
        glBindFramebuffer(this->toGLType(mode), 0);
    }

    RenderTargetStatus RenderTargetUnit::checkStatus(GLResource fbo, RenderTargetMode mode)
    {
        RenderTargetStatus ret = RenderTargetStatus::_UNKNOWN;

        glBindRenderbuffer(GL_RENDERBUFFER, fbo);
        GLenum status = glCheckFramebufferStatus(this->toGLType(mode));

        ret = this->toDriverUnitType(status);

        glBindRenderbuffer(GL_RENDERBUFFER, 0);

        return ret;
    }

    GLenum RenderTargetUnit::toGLType(RenderTargetAttachmentFormat type)
    {
        GLenum ret = 0;

        switch (type)
        {
            case RenderTargetAttachmentFormat::DEPTH_COMPONENT_16:
            {
                ret = GL_DEPTH_COMPONENT16;
            }
            break;

            case RenderTargetAttachmentFormat::DEPTH_COMPONENT_32:
            {
                ret = GL_DEPTH_COMPONENT32;
            }
            break;

            case RenderTargetAttachmentFormat::RGB565:
            {
                ret = GL_RGB565;
            }
            break;

            case RenderTargetAttachmentFormat::RGB5_A1:
            {
                ret = GL_RGB5_A1;
            }
            break;

            case RenderTargetAttachmentFormat::RGBA4:
            {
                ret = GL_RGBA4;
            }
            break;

            case RenderTargetAttachmentFormat::RGBA8:
            {
                ret = GL_RGBA8;
            }
            break;

            case RenderTargetAttachmentFormat::STENCIL_INDEX_8:
            {
                ret = GL_STENCIL_INDEX8;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented RenderTargetAttachmentFormat");
        }

        return ret;
    }

    GLenum RenderTargetUnit::toGLType(RenderTargetAttachmentType type)
    {
        GLenum ret = 0;

        if (RenderTargetAttachmentType::DEPTH_ATTACHMENT == type)
        {
            ret = GL_DEPTH_ATTACHMENT;
        }
        else
        {
            if ((int32_t)RenderTargetAttachmentType::COLOR_ATTACHMENT_0 <= (int32_t)type || (int32_t)RenderTargetAttachmentType::COLOR_ATTACHMENT_15 <= (int32_t)type)
            {
                int32_t i = ((int32_t)type - (int32_t)RenderTargetAttachmentType::COLOR_ATTACHMENT_0);
                ret = GL_COLOR_ATTACHMENT0 + i;
            }
#ifdef _DEBUG
            else
            {
                ASSERT_FALSE("Not implemented RenderTargetAttachmentType");
            }
#endif
        }

 
        return ret;
    }

    GLenum RenderTargetUnit::toGLType(RenderTargetMode mode)
    {
        GLenum ret = 0;

        switch (mode)
        {
            case RenderTargetMode::DRAW:
            {
                ret = GL_DRAW_FRAMEBUFFER;
            }
            break;

            case RenderTargetMode::READ:
            {
                ret = GL_READ_FRAMEBUFFER;
            }
            break;

            case RenderTargetMode::DRAW_READ:
            {
                ret = GL_FRAMEBUFFER;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented RenerTargetMode");
        }

        return ret;
    }

    RenderTargetStatus RenderTargetUnit::toDriverUnitType(GLenum type)
    {
        RenderTargetStatus ret = RenderTargetStatus::_UNKNOWN;

        switch (type)
        {
            case GL_FRAMEBUFFER_UNDEFINED:
            {
                ret = RenderTargetStatus::UNDEFINED;
            }
            break;

            case GL_FRAMEBUFFER_COMPLETE:
            {
                ret = RenderTargetStatus::OK;
            }
            break;

            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            {
                ret = RenderTargetStatus::INCOMPLETE_ATTACHMENT;
            }
            break;

            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            {
                ret = RenderTargetStatus::MISSING_ATTACHMENT;
            }
            break;

            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
            {
                ret = RenderTargetStatus::INCOMPLETE_DRAW_BUFFER;
            }
            break;

            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
            {
                ret = RenderTargetStatus::INCOMPLETE_READ_BUFFER;
            }
            break;

            case GL_FRAMEBUFFER_UNSUPPORTED:
            {
                ret = RenderTargetStatus::UNSUPPORTED;
            }
            break;

            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
            {
                ret = RenderTargetStatus::INCOMPLETE_MULTISAMPLE;
            }
            break;

            case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
            {
                ret = RenderTargetStatus::INCOMPLETE_LAYER_TARGETS;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemtned GL RenderTarget error");
        }

        return ret;
    }
}
}
}