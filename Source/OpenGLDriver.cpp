/* OpenGLDriver.cpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "OpenGLDriver.hpp"

#include <Logger.hpp>
#include <Assert.hpp>

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    using namespace common;
    using namespace math;

 
    bool OpenGLDriver::init()
    {
        bool ret = false;

        glewExperimental = GL_TRUE;
        int32_t glewInitResult = glewInit();
        if (GLEW_OK == glewInitResult && GLEW_VERSION_3_3)
        {
            this->context.program = -1;
            this->context.vao = -1;

            this->pTextureUnit = new TextureUnit(&(this->context));
            this->pShaderUnit = new ShaderUnit(&(this->context));
            this->pGeometryBufferUnit = new GeometryBufferUnit(&(this->context));

            ret = true;
        }

        this->clearErrors();

        return ret;
    }

    void OpenGLDriver::release()
    {
        delete this->pTextureUnit; this->pTextureUnit = nullptr;
        delete this->pShaderUnit; this->pShaderUnit = nullptr;
        delete this->pGeometryBufferUnit; this->pGeometryBufferUnit = nullptr;
        delete this->pRenderTargetUnit; this->pRenderTargetUnit = nullptr;
    }

    void OpenGLDriver::clear(Color4f color)
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(color.r, color.g, color.b, color.a);
    }

    void OpenGLDriver::draw(DrawingMode mode, GLResource program, GLResource vao, int32_t verticesCount)
    {
        if (program != this->context.program)
        {
            this->context.program = program;
            glUseProgram(this->context.program);
        }

        if (vao != this->context.vao)
        {
            this->context.vao = vao;
            glBindVertexArray(this->context.vao);
        }
        
        glDrawElements(this->toGLType(mode), verticesCount, GL_UNSIGNED_SHORT, nullptr);
    }

    void OpenGLDriver::draw(GLResource fbo, const std::vector<RenderTargetAttachmentType> &colorAttachments, int32_t width, int32_t height, bool isDepthBuffer, bool isStencilBuffer)
    {
        glDrawBuffer(GL_BACK);    

        GLenum copyFlags = GL_COLOR_BUFFER_BIT | (true == isDepthBuffer ? GL_DEPTH_BUFFER_BIT : 0) | (true == isStencilBuffer ? GL_STENCIL_BUFFER_BIT : 0);

        for (int32_t i = 0; i < (int32_t)colorAttachments.size(); i++)
        {
            OpenGLDriver::getInstance().pRenderTargetUnit->begin(fbo, RenderTargetMode::READ);
            glReadBuffer(GL_COLOR_ATTACHMENT0 + i); 
            glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, copyFlags, GL_NEAREST);
        }
    }

    void OpenGLDriver::setViewport(int32_t x, int32_t y, int32_t width, int32_t height)
    {
        glViewport(x, y, width, height);
    }

    void OpenGLDriver::setBlendingMode(BlendingFactor sFactor, BlendingFactor dFactor)
    {
        glBlendFunc(this->toGLType(sFactor), this->toGLType(dFactor));
    }

    void OpenGLDriver::enable(DriverCapability capability)
    {
        if (DriverCapability::WRITE_TO_DEPTH_BUFFER != capability)
        {
            glEnable(this->toGLType(capability));
        }
        else
        {
            glDepthMask(GL_TRUE);
        }
    }

    void OpenGLDriver::disable(DriverCapability capability)
    {
        if (DriverCapability::WRITE_TO_DEPTH_BUFFER != capability)
        {
            glDisable(this->toGLType(capability));
        }
        else
        {
            glDepthMask(GL_FALSE);
        }
    }

    void OpenGLDriver::setPointSize(float size)
    {
        glPointSize(size);
    }

    void OpenGLDriver::setLineWidth(float width)
    {
        glLineWidth(width);
    }

    int32_t OpenGLDriver::getProperty(DriverProperty property)
    {
        int32_t ret = -1;

        switch (property)
        {
            case DriverProperty::MAX_FRAMEBUFFER_COLOR_ATTACHMENTS:
            {
                glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &ret);
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented DriverProperty case");
        }

        return ret;
    }

    DriverError OpenGLDriver::getLastError()
    {
        DriverError ret = DriverError::_UNKNOWN;

        switch (glGetError())
        {
            case GL_NO_ERROR:
            {
                ret = DriverError::NO_ERROR;
            }
            break;

            case GL_INVALID_ENUM:
            {
                ret = DriverError::INVALID_ENUM;
            }
            break;

            case GL_INVALID_VALUE:
            {
                ret = DriverError::INVALID_VALUE;
            }
            break;

            case GL_INVALID_OPERATION:
            {
                ret = DriverError::INVALID_OPERATION;
            }
            break;

            case GL_INVALID_FRAMEBUFFER_OPERATION:
            {
                ret = DriverError::INVALID_FRAMEBUFFER_OPERATION;
            }
            break;

            case GL_OUT_OF_MEMORY:
            {
                ret = DriverError::OUT_OF_MEMORY;
            }
            break;

            case GL_STACK_UNDERFLOW:
            {
                ret = DriverError::STACK_UNDERFLOW;
            }
            break;

            case GL_STACK_OVERFLOW:
            {
                ret = DriverError::STACK_UNDERFLOW;
            }
            break;
        }

        return ret;
    }

    void OpenGLDriver::clearErrors()
    {
        glGetError();
    }

    GLenum OpenGLDriver::toGLType(BlendingFactor factor) const
    {
        GLenum ret = 0;

        switch (factor)
        {
            case BlendingFactor::CONSTANT_ALPHA:
            {
                ret = GL_CONSTANT_ALPHA;
            }
            break;

            case BlendingFactor::CONSTANT_COLOR:
            {
                ret = GL_CONSTANT_COLOR;
            }
            break;

            case BlendingFactor::DST_ALPHA:
            {
                ret = GL_DST_ALPHA;
            }
            break;

            case BlendingFactor::DST_COLOR:
            {
                ret = GL_DST_COLOR;
            }
            break;

            case BlendingFactor::ONE:
            {
                ret = GL_ONE;
            }
            break;

            case BlendingFactor::ONE_MINUS_CONSTANT_ALPHA:
            {
                ret = GL_ONE_MINUS_CONSTANT_ALPHA;
            }
            break;

            case BlendingFactor::ONE_MINUS_CONSTANT_COLOR:
            {
                ret = GL_ONE_MINUS_CONSTANT_COLOR;
            }
            break;

            case BlendingFactor::ONE_MINUS_DST_ALPHA:
            {
                ret = GL_ONE_MINUS_DST_ALPHA;
            }
            break;

            case BlendingFactor::ONE_MINUS_DST_COLOR:
            {
                ret = GL_ONE_MINUS_DST_COLOR;
            }
            break;

            case BlendingFactor::ONE_MINUS_SRC_ALPHA:
            {
                ret = GL_ONE_MINUS_SRC_ALPHA;
            }
            break;

            case BlendingFactor::ONE_MINUS_SRC_COLOR:
            {
                ret = GL_ONE_MINUS_SRC_COLOR;
            }
            break;

            case BlendingFactor::SRC_ALPHA:
            {
                ret = GL_SRC_ALPHA;
            }
            break;

            case BlendingFactor::SRC_COLOR:
            {
                ret = GL_SRC_COLOR;
            }
            break;

            case BlendingFactor::ZERO:
            {
                ret = GL_ZERO;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented BlendingFactor");
        }

        return ret;
    }

    GLenum OpenGLDriver::toGLType(DrawingMode mode) const
    {
        GLenum ret = 0;
        switch (mode)
        {
            case DrawingMode::LINES:
            {
                ret = GL_LINES;
            }
            break;

            case DrawingMode::POINTS:
            {
                ret = GL_POINTS;
            }
            break;

            case DrawingMode::TRIANGLES:
            {
                ret = GL_TRIANGLES;
            }
            break;

            case DrawingMode::LINE_LOOP:
            {
                ret = GL_LINE_LOOP;
            }
            break;

            case DrawingMode::TRIANGLE_STRIP:
            {
                ret = GL_TRIANGLE_STRIP;
            }
            break;

            case DrawingMode::TRIANGLE_FAN:
            {
                ret = GL_TRIANGLE_FAN;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented DrawinMode case");
        }

        return ret;
    }

    GLenum OpenGLDriver::toGLType(VertexAttributeType type) const
    {
        GLenum ret = 0;

        switch (type)
        {
            case VertexAttributeType::INT16:
            {
                ret = GL_SHORT;
            }
            break;

            case VertexAttributeType::UNSIGNED_INT16:
            {
                ret = GL_UNSIGNED_SHORT;
            }
            break;

            case VertexAttributeType::INT32:
            {
                ret = GL_INT;
            }
            break;

            case VertexAttributeType::UNSIGNED_INT32:
            {
                ret = GL_UNSIGNED_INT;
            }
            break;

            case VertexAttributeType::FLOAT:
            {
                ret = GL_FLOAT;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented vertex attribute type");
        }

        return ret;

    }

    GLenum OpenGLDriver::toGLType(DriverCapability capability) const
    {
        GLenum ret = 0;

        switch (capability)
        {
            case DriverCapability::DEPTH_TEST:
            {
                ret = GL_DEPTH_TEST;
            }
            break;

            case DriverCapability::BLEND:
            {
                ret = GL_BLEND;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented Driver Capability");
        }

        return ret;
    }
}
}
}