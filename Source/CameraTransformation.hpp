#pragma once

/* CameraTransformation.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <Matrix4x4.hpp>
#include <Frustum.hpp>
#include <Vector2.hpp>
#include <Vector4.hpp>

namespace dynamicreflectance {
namespace multicolor
{

    class CameraTransformation
    {
    private:

        struct Status
        {
            union
            {
                struct
                {
                    uint16_t viewChanged : 1;
                    uint16_t projectionChanged : 1;
                    uint16_t frustumChanged : 1;
                };
                uint16_t flags;
            };

            Status()
                : flags(0)
            {}

            Status(uint16_t viewChanged, uint16_t projectionChanged, uint16_t frustumChanged)
                : viewChanged(viewChanged)
                , projectionChanged(projectionChanged)
                , frustumChanged(frustumChanged)
            {}
        };

    public:

        CameraTransformation()
            : position(0.0f, 0.0f, 0.0f, 1.0f)
            , angleInRadians(0.0f)
            , projectionLeftRight(0.0f, 0.0f)
            , projectionTopBottom(0.0f, 0.0f)
            , projectionFarNear(0.0f, 0.0f)
            , viewMatrix(1.0f)
            , projectionMatrix(1.0f)
            , projectionViewMatrix(1.0f)
        {}

        CameraTransformation(const CameraTransformation &rhs)
            : position(rhs.position)
            , angleInRadians(rhs.angleInRadians)
            , projectionFarNear(rhs.projectionFarNear)
            , viewMatrix(rhs.viewMatrix)
            , projectionMatrix(rhs.projectionMatrix)
            , projectionViewMatrix(rhs.projectionViewMatrix)
            , frustum(rhs.frustum)
        {}

        CameraTransformation(common::math::Vector3 position, float angleInRadians, common::math::Vector2 topBottom, common::math::Vector2 leftRight, common::math::Vector2 farNear)
            : position(position.x, position.y, position.z, 1.0f)
            , angleInRadians(angleInRadians)
            , projectionFarNear(farNear)
            , projectionLeftRight(leftRight)
            , projectionTopBottom(topBottom)
            , viewMatrix(1.0f)
            , projectionMatrix(1.0f)
            , projectionViewMatrix(1.0f)
            , status(1, 1, 1)
        {}

        virtual ~CameraTransformation()
        {}

        void setView(const common::math::Vector3 &position, float angleInRadians)
        {
            this->angleInRadians = angleInRadians;
            this->position.xyz = position;
            this->status.viewChanged = 1;
            this->status.frustumChanged = 1;
        }

        void setPosition(const common::math::Vector3 &position)
        {
            this->position.xyz = position;
            this->status.viewChanged = 1;
            this->status.frustumChanged = 1;
        }

        void setAngle(float angleInRadians)
        {
            this->angleInRadians = angleInRadians;
            this->status.viewChanged = 1;
        }

        void setProjection(const common::math::Vector2 &leftRight, const common::math::Vector2 &topBottom, const common::math::Vector2 &farNear)
        {
            this->projectionLeftRight = leftRight;
            this->projectionTopBottom = topBottom;

            this->projectionFarNear = farNear;
            this->status.projectionChanged = 1;
            this->status.frustumChanged = 1;
        }

        common::math::Vector4 getPosition() const
        {
            return this->position;
        }

        float getAngleInRadians() const
        {
            return this->angleInRadians;
        }

        virtual const common::math::Matrix4x4& getCalculateProjectionViewMatrix() = 0;
        virtual const common::math::Frustum& getCalculateFrustum() = 0;

    protected:

        common::math::Vector4 position;
        float angleInRadians;

        common::math::Vector2 projectionLeftRight;
        common::math::Vector2 projectionTopBottom;
        common::math::Vector2 projectionFarNear;

        common::math::Matrix4x4 viewMatrix;
        common::math::Matrix4x4 projectionMatrix;
        common::math::Matrix4x4 projectionViewMatrix;

        common::math::Frustum frustum;

        Status status;
    };

}
}