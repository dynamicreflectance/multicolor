#pragma once

/* Spritesheet.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Renderable.hpp"
#include "Texture.hpp"

#include <Color4.hpp>

namespace dynamicreflectance {
namespace multicolor
{

    class Spritesheet : public Renderable
    {
    public:

        Spritesheet(const Texture &texture, common::Color4f color, const ModelTransformation &transformation, int32_t frameArrayWidth, int32_t frameArrayHeight, float depth, BHVTreeTransformationCallback *pCallback)
            : Renderable(RenderableType::SPRITESHEET, color, transformation, depth, pCallback)
            , texture(texture)
            , frameArrayWidth(frameArrayWidth)
            , frameArrayHeight(frameArrayHeight)
            , actualFrame(0)
        {}

        bool setFrame(int32_t frame)
        {
            bool ret = frame < this->frameArrayWidth * this->frameArrayHeight;

            if (true == ret)
            {
                this->actualFrame = frame;
            }

            return ret;
        }

        void setNextFrame(bool repeat = true)
        {
            this->actualFrame++;
            int32_t frameDimmensions = this->frameArrayWidth * this->frameArrayHeight;
            if (true == repeat)
            {
                if (this->actualFrame >= frameDimmensions)
                {
                    this->actualFrame = 0;
                }
            }
            else
            {
                if (this->actualFrame >= frameDimmensions)
                {
                    this->actualFrame = frameDimmensions - 1;
                }
            }
        }

        Texture getTexture() const
        {
            return this->texture;
        }

        int32_t getFrameArrayWidth() const
        {
            return this->frameArrayWidth;
        }

        int32_t getFrameArrayHeight() const
        {
            return this->frameArrayHeight;
        }

        int32_t getActualFrame() const
        {
            return this->actualFrame;
        }

        int32_t getFramesCount() const
        {
            return this->frameArrayWidth * this->frameArrayHeight;
        }

    private:

        friend class Scene;

        Texture texture;

        int32_t frameArrayWidth;
        int32_t frameArrayHeight;
        int32_t actualFrame;
    };

}
}