/* TextureUnit.cpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "TextureUnit.hpp"

#include <Assert.hpp>

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{

    using namespace common;

    GLResource TextureUnit::create(const uint8_t *pData, ColorFormat colorFormat, int32_t width, int32_t height, TextureFilterType magFilter, TextureFilterType minFilter, TextureWrappingMode wrapMode)
    {
        GLResource ret = 0;

        glGenTextures(1, &ret);
        glBindTexture(GL_TEXTURE_2D, ret);

        this->setData(ret, pData, colorFormat, width, height);
        this->setFilters(ret, magFilter, minFilter);
        this->setWrappingMode(ret, wrapMode);

        return ret;
    }

    void TextureUnit::setActive(GLResource texture, int32_t id)
    {
        glActiveTexture(GL_TEXTURE0 + id);
        glBindTexture(GL_TEXTURE_2D, texture);
    }

    void TextureUnit::setFilters(GLResource texture, TextureFilterType magFilter, TextureFilterType minFilter)
    {
        ASSERT(magFilter == TextureFilterType::LINEAR || magFilter == TextureFilterType::NEAREST, "Unsupported mag filter type");

        glBindTexture(GL_TEXTURE_2D, texture);

        GLenum glMagFilter = this->toGLType(magFilter);
        GLenum glMinFilter = this->toGLType(minFilter);

        if (GL_NEAREST_MIPMAP_NEAREST == glMinFilter ||
            GL_NEAREST_MIPMAP_LINEAR == glMinFilter ||
            GL_LINEAR_MIPMAP_NEAREST == glMinFilter ||
            GL_LINEAR_MIPMAP_LINEAR == glMinFilter)
        {
            glGenerateMipmap(GL_TEXTURE_2D);
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, glMagFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glMinFilter);

        glBindTexture(GL_TEXTURE_2D, 0);
    }

    void TextureUnit::setWrappingMode(GLResource texture, TextureWrappingMode wrappingMode)
    {
        glBindTexture(GL_TEXTURE_2D, texture);

        GLenum glWrappingMode = this->toGLType(wrappingMode);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glWrappingMode);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glWrappingMode);

        glBindTexture(GL_TEXTURE_2D, 0);
    }

    void TextureUnit::setData(GLResource texture, const uint8_t *pData, ColorFormat colorFormat, int32_t width, int32_t height)
    {
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, this->toGLType(colorFormat), GL_UNSIGNED_BYTE, pData);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    void TextureUnit::release(InOut<GLResource> texture)
    {
        glDeleteTextures(1, texture.getPointer());
        texture.getValue() = 0;
    }


    GLenum TextureUnit::toGLType(TextureWrappingMode mode) const
    {
        GLenum ret = 0;

        switch (mode)
        {
            case TextureWrappingMode::CLAMP_TO_EDGE:
            {
                ret = GL_CLAMP_TO_EDGE;
            }
            break;

            case TextureWrappingMode::MIRRORED_REPEAT:
            {
                ret = GL_MIRRORED_REPEAT;
            }
            break;

            case TextureWrappingMode::REPEAT:
            {
                ret = GL_REPEAT;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented TextureWrappingMode");
        }

        return ret;
    }

    GLenum TextureUnit::toGLType(TextureFilterType filter) const
    {
        GLenum ret = 0;

        switch (filter)
        {
            case TextureFilterType::LINEAR:
            {
                ret = GL_LINEAR;
            }
            break;

            case TextureFilterType::NEAREST:
            {
                ret = GL_NEAREST;
            }
            break;

            case TextureFilterType::LINEAR_MIPMAP_NEAREST:
            {
                ret = GL_LINEAR_MIPMAP_NEAREST;
            }
            break;

            case TextureFilterType::LINEAR_MIPMAP_LINEAR:
            {
                ret = GL_LINEAR_MIPMAP_LINEAR;
            }
            break;

            case TextureFilterType::NEAREST_MIPMAP_NEAREST:
            {
                ret = GL_NEAREST_MIPMAP_NEAREST;
            }
            break;

            case TextureFilterType::NEAREST_MIPMAP_LINEAR:
            {
                ret = GL_NEAREST_MIPMAP_LINEAR;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented TextureFilterType");
        }

        return ret;
    }

    GLenum TextureUnit::toGLType(ColorFormat format) const
    {
        GLenum ret = 0;

        switch (format)
        {
            case ColorFormat::RGB:
            {
                ret = GL_RGB;
            }
            break;

            case ColorFormat::RGBA:
            {
                ret = GL_RGBA;
            }
            break;

            case ColorFormat::DEPTH:
            {
                ret = GL_DEPTH_COMPONENT;
            }
            break;

            case ColorFormat::RED:
            {
                ret = GL_RED;
            }
            break;

            DEFAULT_CASE_ASSERT("Not implemented ImageColorFormat");
        }

        return ret;
    }

}
}
}