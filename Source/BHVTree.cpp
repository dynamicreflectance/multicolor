/* BHVTree.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "BHVTree.hpp" 
#include "Renderable.hpp"

#include <Frustum.hpp>

namespace dynamicreflectance {
namespace multicolor
{
    using namespace common;
    using namespace math;

    int32_t BHVTree::addLeaf(Renderable *pRenderable)
    {
        ASSERT(false == this->nodes.empty(), "Nodes vector is empty!");
        pRenderable->id = this->addLeaf(this->rootId, pRenderable);
        return pRenderable->id;
    }

    void BHVTree::removeLeaf(int32_t id)
    {
        if (true == this->nodes[id].hasChildren() || true == this->nodes[id].hasParent())
        {
            int32_t leafParentId = this->nodes[id].parentId;
            int32_t leafGrandParentId = this->nodes[leafParentId].parentId;
            int32_t leafSiblingId = id == this->nodes[leafParentId].leftChildId ? this->nodes[leafParentId].rightChildId : this->nodes[leafParentId].leftChildId;

            if (NULL_NODE != leafGrandParentId)
            {
                this->nodes[leafSiblingId].parentId = leafGrandParentId;
                if (leafParentId == this->nodes[leafGrandParentId].leftChildId) this->nodes[leafGrandParentId].leftChildId = leafSiblingId;
                if (leafParentId == this->nodes[leafGrandParentId].rightChildId) this->nodes[leafGrandParentId].rightChildId = leafSiblingId;

                this->updateBHVNodeParent(leafParentId);
            }
            else
            {
                this->nodes[leafSiblingId].parentId = NULL_NODE;

                this->nodes[leafParentId].leftChildId = NULL_NODE;
                this->nodes[leafParentId].rightChildId = NULL_NODE;
                this->nodes[leafParentId].parentId = NULL_NODE;

                this->nodesFreeList.push_back(leafParentId);

                this->rootId = leafSiblingId;
            }

            this->nodes[id].leftChildId = NULL_NODE;
            this->nodes[id].rightChildId = NULL_NODE;
            this->nodes[id].parentId = NULL_NODE;

            this->dataFreeList.push_back(this->nodes[id].dataId);
            this->nodesFreeList.push_back(id);
        }
        else
        {
            this->nodes[this->rootId].leftChildId = NULL_NODE;
            this->nodes[this->rootId].rightChildId = NULL_NODE;
            this->nodes[this->rootId].dataId = NULL_DATA;
            this->nodes[this->rootId].aabb.center.setZero();
            this->nodes[this->rootId].aabb.halfSize.set(-1.0f, -1.0f, -1.0f);
        }
    }

    void BHVTree::moveLeaf(int32_t id, const AABB3 &newAabb)
    {
        bool c = contains(this->nodes[id].aabb, newAabb);
        if (false == c)
        {
            Renderable *pData = this->data[this->nodes[id].dataId];

            this->removeLeaf(id);
            pData->id = this->addLeaf(pData);
        }
    }

    std::vector<common::math::AABB3> BHVTree::getCalculateNodes(const Frustum &frustum) const
    {
        std::vector<AABB3> ret;

        if (true == this->nodes[this->rootId].hasChildren() || true == this->nodes[this->rootId].hasData())
        {
            this->traverseBuffer.clear();
            int32_t c = 0;

            this->traverseBuffer.push_back(this->rootId);

            while (c < (int32_t)this->traverseBuffer.size())
            {
                int32_t visitedNodeId = this->traverseBuffer[c];

                if (true == test(frustum, this->nodes[visitedNodeId].aabb))
                {
                    if (true == this->nodes[visitedNodeId].hasChildren())
                    {
                        this->traverseBuffer.push_back(this->nodes[visitedNodeId].leftChildId);
                        this->traverseBuffer.push_back(this->nodes[visitedNodeId].rightChildId);
                    }
   
                    ret.push_back(this->nodes[visitedNodeId].aabb);
                }

                c++;
            }
        }
        
        return ret;
    }

    std::vector<const Renderable*> BHVTree::getCalculateLeaves(const Frustum &frustum) const
    {
        std::vector<const Renderable*> ret;

        if (true == this->nodes[this->rootId].hasChildren() || true == this->nodes[this->rootId].hasData())
        {
            this->traverseBuffer.clear();
            int32_t c = 0;

            this->traverseBuffer.push_back(this->rootId);

            while (c < (int32_t)this->traverseBuffer.size())
            {
                int32_t visitedNodeId = this->traverseBuffer[c];

                if (true == test(frustum, this->nodes[visitedNodeId].aabb))
                {
                    if (true == this->nodes[visitedNodeId].hasChildren())
                    {
                        this->traverseBuffer.push_back(this->nodes[visitedNodeId].leftChildId);
                        this->traverseBuffer.push_back(this->nodes[visitedNodeId].rightChildId);
                    }
                    else
                    {
                        int32_t dataId = this->nodes[visitedNodeId].dataId;
                        ret.push_back(this->data[dataId]);
                    }
                }

                c++;
            }
        }

        return ret;
    }

    int32_t BHVTree::addLeaf(int32_t parentId, Renderable *pRenderable)
    {
        int32_t ret = NULL_NODE;
        if (true == this->nodes[parentId].hasChildren())
        {
            int32_t leftChildId = this->nodes[parentId].leftChildId;
            int32_t rightChildId = this->nodes[parentId].rightChildId;

            AABB3 aabb = pRenderable->getCalculateAABB3();

            float a = area(AABB3(this->nodes[leftChildId].aabb, aabb));
            float b = area(AABB3(this->nodes[rightChildId].aabb, aabb));

            if (a < b)
            {
                ret = this->addLeaf(leftChildId, pRenderable);
            }
            else
            {
                ret = this->addLeaf(rightChildId, pRenderable);
            }
        }
        else
        {
            if (true == this->nodes[parentId].hasData())
            {
                int32_t newParentId = this->allocateNode();
                int32_t newLeftChildId = this->allocateNode();

                this->nodes[newLeftChildId].parentId = newParentId;
                this->nodes[newLeftChildId].aabb = pRenderable->getCalculateAABB3(this->aabbNodeFatFactor);

                this->nodes[newParentId].parentId = this->nodes[parentId].parentId;
                this->nodes[newParentId].leftChildId = newLeftChildId;
                this->nodes[newParentId].rightChildId = parentId;
                this->nodes[newParentId].aabb = AABB3(this->nodes[newLeftChildId].aabb, this->nodes[parentId].aabb);

                int32_t grandParentId = this->nodes[parentId].parentId;
                if (NULL_NODE != grandParentId)
                {
                    if (parentId == this->nodes[grandParentId].leftChildId) this->nodes[grandParentId].leftChildId = newParentId;
                    if (parentId == this->nodes[grandParentId].rightChildId) this->nodes[grandParentId].rightChildId = newParentId;
                }

                this->nodes[parentId].parentId = newParentId;
                if (true == this->nodes[newParentId].hasParent())
                {
                    this->updateBHVNodeParent(this->nodes[newParentId].parentId);
                }

                this->nodes[newLeftChildId].dataId = this->allocateData(pRenderable);
                ret = newLeftChildId;
            }
            else
            {
                this->nodes[parentId].aabb = pRenderable->getCalculateAABB3();
                this->nodes[parentId].dataId = (int32_t)this->data.size();
                this->data.push_back(pRenderable);
                ret = parentId;
            }
        }

        this->findRoot();

        return ret;
    }

    int32_t BHVTree::allocateNode()
    {
        int32_t ret = NULL_NODE;

        if (false == this->nodesFreeList.empty())
        {
            ret = this->nodesFreeList.back();
            this->nodesFreeList.pop_back();
        }
        else
        {
            this->nodes.push_back(BHVNode());
            ret = (int32_t) this->nodes.size() - 1;
        }

        return ret;
    }

    int32_t BHVTree::allocateData(Renderable *pRenderable)
    {
        int32_t ret = NULL_DATA;

        if (false == this->dataFreeList.empty())
        {
            ret = this->dataFreeList.back();
            this->dataFreeList.pop_back();

            this->data[ret] = pRenderable;
        }
        else
        {
            this->data.push_back(pRenderable);
            ret = (int32_t)this->data.size() - 1;
        }

        return ret;
    }

    void BHVTree::updateBHVNodeParent(int32_t parentId)
    {
        int32_t leftChildId = this->nodes[parentId].leftChildId;
        int32_t rightChildId = this->nodes[parentId].rightChildId;

        this->nodes[parentId].aabb = AABB3(this->nodes[leftChildId].aabb, this->nodes[rightChildId].aabb);

        if (true == this->nodes[parentId].hasParent())
        {
            this->updateBHVNodeParent(this->nodes[parentId].parentId);
        }
    }

    void BHVTree::findRoot()
    {
        size_t i = 0;
        bool found = false;

        while (i < this->nodes.size() && false == found)
        {
            if (NULL_NODE == this->nodes[i].parentId)
            {
                found = true;
            }

            i++;
        }

        if (true == found)
        {
            this->rootId = (int32_t)i - 1;
        }

        ASSERT(0 <= this->rootId, "Incorrect rootId");
    }

}
}