#pragma once

/* Face.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <cstdint>

namespace dynamicreflectance {
namespace multicolor
{
    struct Face
    {
        int16_t v0, v1, v2;

        Face()
            : v0(0)
            , v1(0)
            , v2(0)
        {}

        Face(int16_t v0, int16_t v1, int16_t v2)
            : v0(v0)
            , v1(v1)
            , v2(v2)
        {}
    };

}
}