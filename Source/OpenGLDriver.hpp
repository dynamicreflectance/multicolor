#pragma once

/* OpenGLDriver.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "OpenGLDriverTypes.hpp"
#include "Context.hpp"
#include "VertexAttribute.hpp"

#include "ShaderUnit.hpp"
#include "TextureUnit.hpp"
#include "GeometryBufferUnit.hpp"
#include "RenderTargetUnit.hpp"

#include <Singleton.hpp>
#include <InOut.hpp>
#include <Vector2.hpp>
#include <Vector3.hpp>
#include <Matrix4x4.hpp>
#include <Color4.hpp>
#include <ColorFormat.hpp>
#include <EntryLocation.hpp>


namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{

    enum class DrawingMode
    {
        _UNKNOWN = -1,
        POINTS,
        LINES,
        TRIANGLES,
        LINE_LOOP,
        TRIANGLE_STRIP,
        TRIANGLE_FAN,
        _COUNT
    };

    enum class BlendingFactor
    {
        _UNKNOWN = -1,
        ZERO,
        ONE,
        SRC_COLOR,
        ONE_MINUS_SRC_COLOR,
        DST_COLOR,
        ONE_MINUS_DST_COLOR,
        SRC_ALPHA,
        ONE_MINUS_SRC_ALPHA,
        DST_ALPHA,
        ONE_MINUS_DST_ALPHA,
        CONSTANT_COLOR,
        ONE_MINUS_CONSTANT_COLOR,
        CONSTANT_ALPHA,
        ONE_MINUS_CONSTANT_ALPHA,
        _COUNT
    };

    enum class DriverCapability
    {
        _UNKNOWN = -1,
        DEPTH_TEST,
        BLEND,
        WRITE_TO_DEPTH_BUFFER,
        _COUNT
    };

    enum class DriverProperty
    {
        _UNKNOWN = -1,
        MAX_FRAMEBUFFER_COLOR_ATTACHMENTS,
        _COUNT
    };

    enum DriverError
    {
        _UNKNOWN = -1,
        NO_ERROR,
        INVALID_ENUM,
        INVALID_VALUE,
        INVALID_OPERATION,
        INVALID_FRAMEBUFFER_OPERATION,
        OUT_OF_MEMORY,
        STACK_UNDERFLOW,
        STACK_OVERFLOW,
        _COUNT
    };


    class OpenGLDriver : public common::Singleton<OpenGLDriver>
    {
    public:

        OpenGLDriver()
            : pTextureUnit(nullptr)
            , pShaderUnit(nullptr)
            , pGeometryBufferUnit(nullptr)
            , pRenderTargetUnit(nullptr)
        {}

        bool init();
        void release();

        void clear(common::Color4f color);
        void draw(DrawingMode mode, GLResource program, GLResource vao, int32_t verticesCount);
        void draw(GLResource fbo, const std::vector<RenderTargetAttachmentType> &colorAttachments, int32_t width, int32_t height, bool isDepthBuffer, bool isStencilBuffer);

        void setViewport(int32_t x, int32_t y, int32_t width, int32_t height);
        void setBlendingMode(BlendingFactor sFactor, BlendingFactor dFactor);

        void enable(DriverCapability capability);
        void disable(DriverCapability capability);

        void setPointSize(float size);
        void setLineWidth(float width);

        int32_t getProperty(DriverProperty property);

        DriverError getLastError();
        void clearErrors();

    public:

        TextureUnit *pTextureUnit;
        ShaderUnit *pShaderUnit;
        GeometryBufferUnit *pGeometryBufferUnit;
        RenderTargetUnit *pRenderTargetUnit;

    private:

        GLenum toGLType(BlendingFactor factor) const;
        GLenum toGLType(DrawingMode mode) const;
        GLenum toGLType(VertexAttributeType type) const;
        GLenum toGLType(DriverCapability capability) const;

    private:

        friend common::Singleton<OpenGLDriver>;

        Context context;
    };

}
}
}