#pragma once

/* Scene.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <AABB3.hpp>

#include <stdint.h>

namespace dynamicreflectance {
namespace multicolor
{

    class BHVTreeTransformationCallback
    {
    public:

        virtual ~BHVTreeTransformationCallback()
        {}

        virtual void transform(int32_t id, const common::math::AABB3 &aabb) = 0;
    };

}
}

