#pragma once

/* Renderer.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "PerspectiveCameraTransformation.hpp"
#include "Renderable.hpp"
#include "OpenGLDriverTypes.hpp"
#include "Settings.hpp"
#include "VertexAttribute.hpp"
#include "BufferUpdateMode.hpp"
#include "HardwareGeometryBuffer.hpp"

#include <Assert.hpp>
#include <Out.hpp>
#include <InOut.hpp>
#include <Result.hpp>
#include <Vector2.hpp>
#include <Resolution.hpp>

#include <vector>

namespace dynamicreflectance {
namespace common
{
    class Logger;
}
}

namespace dynamicreflectance {
namespace multicolor
{
    struct ShaderProgramSource;

    class Sprite;
    class MultitexturedSprite;
    class Spritesheet;
    class DebugDraw;

    class Renderer
    {
    private:

        struct Uniforms
        {
            graphicsdriver::GLResource modelUniform;
            graphicsdriver::GLResource projectionViewUniform;
            graphicsdriver::GLResource colorUniform;
            struct
            {
                graphicsdriver::GLResource sampler;
                graphicsdriver::GLResource framePositionUniform;
                graphicsdriver::GLResource texcoordOffsetUniform;
            }textures[settings::MULTITEXTURE_TEXTURES_COUNT];
        };

        struct RenderCommand
        {
            struct
            {
                graphicsdriver::GLResource handle;

                common::math::Vector2 framePosition;
                common::math::Vector2 frameOffset;
            } textures[settings::MULTITEXTURE_TEXTURES_COUNT];
     
            common::Color4f color;
            ModelTransformation transformation;
        };

        struct RenderTarget
        {
            graphicsdriver::GLResource fbo;
            graphicsdriver::GLResource colorAttachment;
            graphicsdriver::GLResource depthAttachment;
        };
  
    public:

        Renderer(common::Resolution resolution, DebugDraw *pDebugDraw = nullptr, common::Logger *pLogger = nullptr);
        ~Renderer();

        void draw(const std::vector<const Renderable*> &objects, common::InOut<PerspectiveCameraTransformation> camera);

        void draw();

        void begin(const common::Color4f &clearColor);
        void end();

        bool isCreated() const
        {
            return this->created;
        }

    private:

        void fillBuffers(const std::vector<const Renderable*> &objects, common::Out<std::vector<RenderCommand>> opaqueObjectsBuffer, common::Out<std::vector<RenderCommand>> transparentObjectsBuffer);

        friend bool compare(const RenderCommand &a, const RenderCommand &b);

    private:

        graphicsdriver::GLResource defaultTexture;

        std::vector<RenderCommand> opaqueObjects;
        std::vector<RenderCommand> transparentObjects;

        Uniforms uniforms;
        RenderTarget renderTarget;
        HardwareGeometryBuffer geometryBuffer;

        graphicsdriver::GLResource program;
        common::Resolution resolution;
        
        bool created;

        common::Logger *pLogger;
        DebugDraw *pDebugDraw;
    };

}
}