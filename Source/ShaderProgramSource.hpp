#pragma once

/* ShaderProgramSource.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/


#include <string>
#include <vector>

namespace dynamicreflectance {
namespace multicolor
{
    struct ShaderProgramSource
    {
        std::string vertexShader;
        std::string fragmentShader;

        std::vector<std::string> attributes;

        ShaderProgramSource()
        {}

        ShaderProgramSource(const std::string &vertexShader, std::string &fragmentShader, const std::vector<std::string> &attributes)
            : vertexShader(vertexShader)
            , fragmentShader(fragmentShader)
            , attributes(attributes)
        {}
    };


}
}