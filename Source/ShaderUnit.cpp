/* ShaderUnit.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "ShaderUnit.hpp"

#include <Assert.hpp>

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    using namespace common;
    using namespace math;

    GLResource ShaderUnit::createShader(ShaderType type, const std::string &source)
    {
        GLResource ret = 0;
        GLenum glShaderType = this->toGLType(type);
        const char *pShaderSource = source.c_str();

        ret = glCreateShader(glShaderType);
        glShaderSource(ret, 1, &pShaderSource, nullptr);
        glCompileShader(ret);

        return ret;
    }

    GLResource ShaderUnit::createShader(ShaderType type, const std::vector<std::string> &sources)
    {
        GLResource ret = 0;

        const char **pGLSources = new const char*[sources.size()];
        for (size_t i = 0; i < sources.size(); i++)
        {
            pGLSources[i] = sources[i].c_str();
        }

        GLenum glShaderType = this->toGLType(type);

        ret = glCreateShader(glShaderType);
        glShaderSource(ret, (GLsizei)sources.size(), pGLSources, nullptr);
        glCompileShader(ret);

        delete pGLSources; pGLSources = nullptr;

        return ret;
    }

    GLResource ShaderUnit::createShaderProgram(GLResource vertexShader, GLResource fragmentShader, const std::vector<std::string> &attributes)
    {
        GLResource ret = glCreateProgram();

        glAttachShader(ret, vertexShader);
        glAttachShader(ret, fragmentShader);

        for (size_t i = 0; i < attributes.size(); i++)
        {
            glBindAttribLocation(ret, (GLuint)i, attributes[i].c_str());
        }

        glLinkProgram(ret);

        return ret;
    }

    void ShaderUnit::releaseShader(InOut<GLResource> shader)
    {
        glDeleteShader(shader.getValue());
        shader.getValue() = 0;
    }

    void ShaderUnit::releaseShaderProgram(InOut<GLResource> program)
    {
        glDeleteProgram(program.getValue());
        program.getValue() = 0;
    }

    bool ShaderUnit::isShaderCreated(GLResource shader)
    {
        GLint status = GL_FALSE;

        glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

        return GL_TRUE == status;
    }

    bool ShaderUnit::isShaderProgramCreated(GLResource program)
    {
        GLint status = GL_FALSE;

        glGetProgramiv((GLuint)program, GL_LINK_STATUS, &status);

        return GL_TRUE == status;
    }

    std::string ShaderUnit::getShaderErrorMessage(GLResource shader)
    {
        //int32_t errorStringLength = 0;
        //glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &errorStringLength);
        //
        //char *pBuffer = new char[]
        //
        //glGetShaderInfoLog(shader, bufferSize, &errorStringLength, &(buffer[0]));
        //
        //return buffer;

        //return DynamicString<char>();
        return std::string();
    }

    std::string ShaderUnit::getShaderProgramErrorMessage(GLResource program)
    {
  
        //int32_t errorStringLength = 0;
        //glGetProgramiv(program, GL_INFO_LOG_LENGTH, &errorStringLength)
        //
        //glGetProgramInfoLog(program, bufferSize, &errorStringLength, &(buffer[0]));
        //
        //return buffer;
        return std::string();
    }

    void ShaderUnit::setUniformi32(GLResource program, GLResource location, int32_t value)
    {
        if (program != this->pContext->program)
        {
            this->pContext->program = program;
            glUseProgram(this->pContext->program);
        }

        glUniform1i(location, value);
    }

    void ShaderUnit::setUniformf(GLResource program, GLResource location, float value)
    {
        if (program != this->pContext->program)
        {
            this->pContext->program = program;
            glUseProgram(this->pContext->program);
        }

        glUniform1f(location, value);
    }

    void ShaderUnit::setUniformVector2(GLResource program, GLResource location, Vector2 value)
    {
        if (program != this->pContext->program)
        {
            this->pContext->program = program;
            glUseProgram(this->pContext->program);
        }

        glUniform2f(location, value.x, value.y);
    }

    void ShaderUnit::setUniformVector3(GLResource program, GLResource location, Vector3 value)
    {
        if (program != this->pContext->program)
        {
            this->pContext->program = program;
            glUseProgram(this->pContext->program);
        }

        glUniform3f(location, value.x, value.y, value.z);
    }

    void ShaderUnit::setUniformVector4(GLResource program, GLResource location, Vector4 value)
    {
        if (program != this->pContext->program)
        {
            this->pContext->program = program;
            glUseProgram(this->pContext->program);
        }

        glUniform4f(location, value.x, value.y, value.z, value.w);
    }

    void ShaderUnit::setUniformMatrix4x4(GLResource program, GLResource location, const Matrix4x4 &value)
    {
        if (program != this->pContext->program)
        {
            this->pContext->program = program;
            glUseProgram(this->pContext->program);
        }

        glUniformMatrix4fv(location, 1, GL_FALSE, &(value.linear[0]));
    }

    void ShaderUnit::setUniformColor4f(GLResource program, GLResource location, Color4f value)
    {
        if (program != this->pContext->program)
        {
            this->pContext->program = program;
            glUseProgram(this->pContext->program);
        }

        glUniform4f(location, value.r, value.g, value.b, value.a);
    }

    GLResource ShaderUnit::getUniformLocation(GLResource program, const std::string &name)
    {
        return glGetUniformLocation(program, name.c_str());
    }

    bool ShaderUnit::isUniform(GLResource program, const std::string &name)
    {
        GLResource location = this->getUniformLocation(program, name);
        return -1 != location;
    }

    GLenum ShaderUnit::toGLType(ShaderType type)
    {
        GLenum ret = 0;

        switch (type)
        {
            case ShaderType::FRAGMENT_SHADER:
                ret = GL_FRAGMENT_SHADER;
            break;

            case ShaderType::GEOMETRY_SHADER:
                ret = GL_GEOMETRY_SHADER;
            break;

            case ShaderType::VERTEX_SHADER:
                ret = GL_VERTEX_SHADER;
            break;

            //DEFAULT_CASE_ASSERT("Not implemented shader type");
        }

        return ret;
    }

}
}
}