#pragma once

/* FileLoader.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Settings.hpp"

#include <Singleton.hpp>
#include <Out.hpp>
#include <ImageData.hpp>

#include <cstdio>

namespace dynamicreflectance {
namespace multicolor
{

    class FileLoader : public common::Singleton<FileLoader>
    {
    private:

        enum class ImageType
        {
            _UNKNOWN = -1,
            PNG,
            _COUNT
        };

    public:

        bool image(const char *fileName, common::Out<common::ImageData> data);
        bool text(const char *fileName, common::Out<std::string> data);
        bool binary(const char *fileName, common::Out<std::vector<uint8_t>> data);

    private:

        ImageType findImageType(const char *fileName);
        
        bool loadPngFile(FILE *fp, common::Out<common::ImageData> data);

    private:

        int8_t buffer[(settings::FILE_LOADER_INTERNAL_BUFFER_SIZE_IN_BYTES / sizeof(int8_t)) - 1];
    };
}
}