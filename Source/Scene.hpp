#pragma once

/* Scene.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "PerspectiveCameraTransformation.hpp"
#include "Sprite.hpp"
#include "Spritesheet.hpp"
#include "MultitexturedSprite.hpp"
#include "BHVTree.hpp"
#include "Settings.hpp"

#include <Assert.hpp>
#include <Math.hpp>
#include <Out.hpp>
#include <InOut.hpp>

#include <vector>

namespace dynamicreflectance {
namespace multicolor
{

    class CameraTransformation;

    class Scene
    {
    private:

        class TransformationCallback : public BHVTreeTransformationCallback
        {
        public:

            TransformationCallback(BHVTree *pTree)
                : pTree(pTree)
            {}

            void transform(int32_t id, const common::math::AABB3 &aabb)
            {
                pTree->moveLeaf(id, aabb);
            }

        private:

            BHVTree *pTree;
        };

    public:

        Scene()
            : tree(settings::FAT_AABB_FACTOR)
            , callback(&(this->tree))
        {}

        ~Scene()
        {}

        Sprite* addSprite(const Texture &texture, common::Color4f color, const ModelTransformation &transformation, float depth)
        {
            Sprite *pSprite = new Sprite(texture, color, transformation, depth, &(this->callback));
            tree.addLeaf(pSprite);

            return pSprite;
        }

        MultitexturedSprite* addMultitexturedSprite(const std::vector<Texture> &textures, common::Color4f color, const ModelTransformation &transformation, float depth)
        {
            MultitexturedSprite *pMultitexturedSprite = new MultitexturedSprite(textures, color, transformation, depth, &(this->callback));
            tree.addLeaf(pMultitexturedSprite);

            return pMultitexturedSprite;
        }

        Spritesheet* addSpritesheet(const Texture &texture, common::Color4f color, const ModelTransformation &transformation, int32_t frameArrayWidth, int32_t frameArrayHeight, float depth)
        {
            Spritesheet *pSpritesheet = new Spritesheet(texture, color, transformation, frameArrayWidth, frameArrayHeight, depth, &(this->callback));
            tree.addLeaf(pSpritesheet);
                        
            return pSpritesheet;
        }

        void remove(common::InOut<Sprite*> object)
        {
            this->tree.removeLeaf(object.getValue()->getId());
            delete object.getValue(); object.getValue() = nullptr;
        }

        void remove(common::InOut<Spritesheet*> object)
        {
            this->tree.removeLeaf(object.getValue()->getId());
            delete object.getValue(); object.getValue() = nullptr;
        }

        void remove(common::InOut<MultitexturedSprite*> object)
        {
            this->tree.removeLeaf(object.getValue()->getId());
            delete object.getValue(); object.getValue() = nullptr;
        }

        std::vector<const Renderable*> getCalculateObjects(common::InOut<PerspectiveCameraTransformation> camera) const
        {
            return this->tree.getCalculateLeaves(camera.getPointer()->getCalculateFrustum());
        }

        std::vector<common::math::AABB3> getCalculateNodes(common::InOut<PerspectiveCameraTransformation> camera)
        {
            return this->tree.getCalculateNodes(camera.getPointer()->getCalculateFrustum());
        }

        int32_t getAllObjectsCount() const
        {
            return this->tree.getCalculateAllObjectsCount();
        }
        

    private:

        BHVTree tree;
        TransformationCallback callback;
    };
}
}
