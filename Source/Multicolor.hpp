#pragma once

/* Multicolor.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "Renderer.hpp"
#include "DebugDraw.hpp"
#include "Sprite.hpp"
#include "Spritesheet.hpp"
#include "MultitexturedSprite.hpp"

#include "Scene.hpp"
#include "PerspectiveCameraTransformation.hpp"

#include "FileLoader.hpp"
#include "FileSystem.hpp"