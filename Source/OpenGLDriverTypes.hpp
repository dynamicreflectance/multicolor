#pragma once

/* OpenGLDriverTypes.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <cstdint>

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    using GLResource = uint32_t;
}
}
}