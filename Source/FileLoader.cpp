/* FileLoader.cpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "FileLoader.hpp"
#include "FileSystem.hpp"

#include <Assert.hpp>

#include "lpng1626\png.h"

#include <cctype>

namespace dynamicreflectance {
namespace multicolor
{
    using namespace common;

    bool FileLoader::image(const char *fileName, Out<ImageData> data)
    {
        Result<std::string, bool> absPathResult = FileSystem::getInstance().findAbsolutePath(fileName, FileSystem::getInstance().getCurrentDir().c_str());

        bool ret = absPathResult.error;

        if (true == ret)
        {
            FILE *fp = fopen(absPathResult.data.c_str(), "rb");
            ret = nullptr != fp;

            if (true == ret)
            {
                ImageType type = findImageType(absPathResult.data.c_str());

                switch (type)
                {
                    case ImageType::PNG:
                    {
                        ret = this->loadPngFile(fp, out(data));
                    }
                    break;

                    case ImageType::_UNKNOWN:
                    {
                        ret = false;
                    }
                    break;

                    DEFAULT_CASE_ASSERT("Not implemented ImageType case");
                }

                fclose(fp);
            }
        }

        return ret;
    }

    bool FileLoader::text(const char *fileName, Out<std::string> data)
    {
        Result<std::string, bool> absPathResult = FileSystem::getInstance().findAbsolutePath(fileName, FileSystem::getInstance().getCurrentDir().c_str());

        bool ret = absPathResult.error;

        if (true == ret)
        {
            FILE *fp = fopen(fileName, "rb");
            ret = nullptr != fp;

            if (true == ret)
            {
                int32_t readedSizeInBytes = 0;
                do
                {
                    readedSizeInBytes = (int32_t)fread(this->buffer, settings::FILE_LOADER_INTERNAL_BUFFER_SIZE_IN_BYTES, 1, fp);
                    this->buffer[readedSizeInBytes] = 0;

                    data.getPointer()->append((char*)this->buffer);

                } while (settings::FILE_LOADER_INTERNAL_BUFFER_SIZE_IN_BYTES != readedSizeInBytes);

                fclose(fp);
            }
        }

        return ret;
    }

    bool FileLoader::binary(const char *fileName, Out<std::vector<uint8_t>> data)
    {
        Result<std::string, bool> absPathResult = FileSystem::getInstance().findAbsolutePath(fileName, FileSystem::getInstance().getCurrentDir().c_str());

        bool ret = absPathResult.error;

        if (true == ret)
        {
            FILE *fp = fopen(fileName, "rb");
            ret = nullptr != fp;

            if (true == ret)
            {
                int32_t readedSizeInBytes = 0;
                do
                {
                    readedSizeInBytes = (int32_t)fread(this->buffer, settings::FILE_LOADER_INTERNAL_BUFFER_SIZE_IN_BYTES, 1, fp);
                    data.getPointer()->insert(data.getPointer()->end(), this->buffer, this->buffer + readedSizeInBytes);

                } while (settings::FILE_LOADER_INTERNAL_BUFFER_SIZE_IN_BYTES != readedSizeInBytes);

                fclose(fp);
            }
        }

        return ret;

    }

    FileLoader::ImageType FileLoader::findImageType(const char *fileName)
    {
        ImageType ret = ImageType::_UNKNOWN;

        int32_t fileNameLength = (int32_t)strlen(fileName);

        if (3 < fileNameLength)
        {
            char extension[3] = { (char)toupper(fileName[fileNameLength - 3]), (char)toupper(fileName[fileNameLength - 2]), (char)toupper(fileName[fileNameLength - 1]) };

            if ('P' == extension[0] && 'N' == extension[1] && 'G' == extension[2])
            {
                ret = ImageType::PNG;
            }
        }

        return ret;
    }

    bool FileLoader::loadPngFile(FILE *fp, Out<ImageData> data)
    {
        png_structp pPngHandle = nullptr;
        png_infop pPngInfo = nullptr;
        png_infop pPngEndInfo = nullptr;
        png_byte header[8] = { 0 };

        bool ret = false;
       
        fread(header, 1, 8, fp);

        ret = 0 == png_sig_cmp(header, 0, 8);

        if (true == ret)
        {
            ret = nullptr != (pPngHandle = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)) && 
                  nullptr != (pPngInfo = png_create_info_struct(pPngHandle)) &&
                  nullptr != (pPngEndInfo = png_create_info_struct(pPngHandle)) &&
                  0 == setjmp(png_jmpbuf(pPngHandle))
            ;
                  
            if (true == ret)
            {
                int32_t bitDepth = 0, colorType = 0;

                png_init_io(pPngHandle, fp);
                png_set_sig_bytes(pPngHandle, 8);
                png_read_info(pPngHandle, pPngInfo);

                png_get_IHDR(pPngHandle, pPngInfo,
                    (png_uint_32*)&(data.getPointer()->resolution.width), (png_uint_32*)&(data.getPointer()->resolution.height),
                    &bitDepth, &colorType, nullptr, nullptr, nullptr
                );

                if (8 == bitDepth)
                {
                    switch (colorType)
                    {
                        case PNG_COLOR_TYPE_RGB:
                        {
                            data.getPointer()->format = ColorFormat::RGB;
                            ret = true;
                        }
                        break;

                        case PNG_COLOR_TYPE_RGB_ALPHA:
                        {
                            data.getPointer()->format = ColorFormat::RGBA;
                            ret = true;
                        }
                        break;

                        default:
                        {
                            ret = false;
                        }
                    }

                    if (true == ret)
                    {
                        png_read_update_info(pPngHandle, pPngInfo);

                        int32_t rowbytes = (int32_t)png_get_rowbytes(pPngHandle, pPngInfo);
                        rowbytes += 3 - ((rowbytes - 1) % 4);

                        data.getPointer()->dataLength = rowbytes * data.getPointer()->resolution.height * sizeof(png_byte) + 15;

                        png_byte *pImageData = (png_byte*)malloc(data.getPointer()->dataLength);
                        png_byte **pRowPointers = (png_byte**)malloc(data.getPointer()->resolution.height * sizeof(png_byte*));

                        for (int32_t i = 0; i < data.getPointer()->resolution.height; i++)
                        {
                            pRowPointers[data.getPointer()->resolution.height - 1 - i] = pImageData + i * rowbytes;
                        }

                        png_read_image(pPngHandle, pRowPointers);
                        data.getPointer()->pData = pImageData;
                    }
                }
            }
        }

        if (nullptr != pPngHandle)
        {
            png_destroy_read_struct(&pPngHandle, nullptr, nullptr);
        }

        if (nullptr != pPngInfo)
        {
            png_destroy_read_struct(nullptr, &pPngInfo, nullptr);
        }

        if (nullptr != pPngEndInfo)
        {
            png_destroy_read_struct(nullptr, nullptr, &pPngEndInfo);
        }

           
        return ret;
    }
}
}