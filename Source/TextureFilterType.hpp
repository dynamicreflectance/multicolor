#pragma once

/* TextureFilterType.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    enum class TextureFilterType
    {
        _UNKNOWN = -1,
        NEAREST,
        LINEAR,

        NEAREST_MIPMAP_NEAREST,
        NEAREST_MIPMAP_LINEAR,
        LINEAR_MIPMAP_NEAREST,
        LINEAR_MIPMAP_LINEAR,
        _COUNT
    };
}
}
}