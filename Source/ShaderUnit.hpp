/* ShaderUnit.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#pragma once

#include "DriverUnit.hpp"
#include "OpenGLDriverTypes.hpp"

#include <InOut.hpp>
#include <Vector2.hpp>
#include <Vector3.hpp>
#include <Vector4.hpp>
#include <Matrix4x4.hpp>
#include <Color4.hpp>

#include <string>
#include <vector>

namespace dynamicreflectance {
namespace multicolor {
namespace graphicsdriver
{
    enum class ShaderType
    {
        _UNKNOWN = -1,
        VERTEX_SHADER,
        FRAGMENT_SHADER,
        GEOMETRY_SHADER,
        _COUNT
    };

    class ShaderUnit : public DriverUnit
    {
    public:

        ShaderUnit(Context *pContext)
            : DriverUnit(pContext)
        {}

        GLResource createShader(ShaderType type, const std::string &source);
        GLResource createShader(ShaderType type, const std::vector<std::string> &sources);
        GLResource createShaderProgram(GLResource vertexShader, GLResource fragmentShader, const std::vector<std::string> &attributes);
        void releaseShader(common::InOut<GLResource> shader);
        void releaseShaderProgram(common::InOut<GLResource> program);

        bool isShaderCreated(GLResource shader);
        bool isShaderProgramCreated(GLResource program);

        std::string getShaderErrorMessage(GLResource shader);
        std::string getShaderProgramErrorMessage(GLResource program);

        void setUniformi32(GLResource program, GLResource location, int32_t value);
        void setUniformf(GLResource program, GLResource location, float value);
        void setUniformVector2(GLResource program, GLResource location, common::math::Vector2 value);
        void setUniformVector3(GLResource program, GLResource location, common::math::Vector3 value);
        void setUniformVector4(GLResource program, GLResource location, common::math::Vector4 value);
        void setUniformMatrix4x4(GLResource program, GLResource location, const common::math::Matrix4x4 &value);
        void setUniformColor4f(GLResource program, GLResource location, common::Color4f value);

        GLResource getUniformLocation(GLResource program, const std::string &name);

        bool isUniform(GLResource program, const std::string &name);

    private:

        GLenum toGLType(ShaderType type);

    private:

        
    };

}
}
}