/* FileSystem.cpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "FileSystem.hpp"
#include "Settings.hpp"

#include <Windows.h>

namespace dynamicreflectance {
namespace multicolor
{
    using namespace common;

    std::string FileSystem::getCurrentDir() const
    {
        static char path[MAX_PATH];

        GetCurrentDirectory(MAX_PATH, path);
        return std::string(path);
    }

    Result<std::string, bool> FileSystem::findAbsolutePath(const char *relativePath, const char *currentDir)
    {
        Result<std::string, bool> ret = { "", true };

        std::vector<Token> relativePathTokens = this->tokenize(relativePath);
        std::vector<Token> currentDirTokens = this->tokenize(currentDir);

        for (int32_t i = 0; i < (int32_t)relativePathTokens.size() && true == ret.error; i++)
        {
            if (true == this->isFolderUpDir(relativePathTokens[i]))
            {
                if (false == currentDirTokens.empty())
                {
                    currentDirTokens.pop_back();
                }
                else
                {
                    ret.error = false;
                }
            }
            else
            {
                currentDirTokens.push_back(relativePathTokens[i]);
            }
        }

        if (true == ret.error)
        {
            ret.data = this->buildString(currentDirTokens);
        }
        
        return ret;
    }

    std::vector<FileSystem::Token> FileSystem::tokenize(const char *string)
    {
        std::vector<Token> ret;

        Token token;
        int32_t lastTokenPosition = 0;
        int32_t stringLength = (int32_t)strlen(string);

        for (int32_t i = 0; i < stringLength; i++)
        {
            if ('\\' == string[i] || '/' == string[i])
            {
                token.pStart = string + lastTokenPosition;
                token.length = i - lastTokenPosition;

                ret.push_back(token);

                lastTokenPosition = i + 1;
            }
        }

        token.pStart = string + lastTokenPosition;
        token.length = stringLength - lastTokenPosition;

        ret.push_back(token);

        return ret;
    }

    bool FileSystem::isFolderUpDir(const Token &token)
    {
        return 2 == token.length && '.' == *(token.pStart + 0) && '.' == *(token.pStart + 1);
    }

    std::string FileSystem::buildString(const std::vector<Token>& tokens)
    {
        std::string ret;

        for (int32_t i = 0; i < (int32_t)tokens.size(); i++)
        {
            for (int32_t l = 0; l < tokens[i].length; l++)
            {
                ret.push_back(*(tokens[i].pStart + l));
            }

            ret.push_back(settings::DEFAULT_FOLDER_SEPARATOR);
        }

        ret.pop_back();

        return ret;
    }

}
}