#pragma once

/* Renderable.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license.  See the LICENSE file for details.
*/


#include "ModelTransformation.hpp"
#include "BHVTreeTransformationCallback.hpp"

#include <AABB3.hpp>
#include <Color4.hpp>
#include <ReferenceCounted.hpp>

#include <cmath>

namespace dynamicreflectance {
namespace multicolor
{

    enum RenderableType
    {
        _UNKNOWN = -1,
        SPRITE,
        MULTITEXTURED_SPRITE,
        SPRITESHEET,
        _COUNT
    };

    class BHVTree;

    class Renderable : public common::ReferenceCounted<Renderable>
    {
    public:

        Renderable(RenderableType type, common::Color4f color, const ModelTransformation &transformation, float depth, BHVTreeTransformationCallback *pCallback)
            : id(-1)
            , type(type)
            , color(color)
            , transformation(transformation)
            , depth(depth)
            , pCallback(pCallback)
        {
            ASSERT(nullptr != pCallback, "Transformation callback cannot be nullptr");
        }

        virtual ~Renderable()
        {
            this->decRef();
        }

        common::math::AABB3 getCalculateAABB3(float fatFactor = 0.0f) const
        {
            float angle = this->transformation.getAngleInRadians();
            common::math::Vector2 size = common::math::Vector2(this->transformation.getScale().x, this->transformation.getScale().y);

            common::math::Vector2 aabbHalfSize2 = common::math::Vector2(
                (size.y * std::abs(sin(angle)) + size.x * std::abs(cos(angle))) * 0.5f,
                (size.x * std::abs(sin(angle)) + size.y * std::abs(cos(angle))) * 0.5f
            );

            return common::math::AABB3(this->transformation.getPosition().xyz, common::math::Vector3(aabbHalfSize2, this->depth * 0.5f), fatFactor);
        }

        void setColor(common::Color4f color)
        {
            this->color = color;
        }

        common::Color4f getColor() const
        {
            return this->color;
        }

        const ModelTransformation& getTransformation() const
        {
            return this->transformation;
        }

        void translate(common::math::Vector3 vec)
        {
            this->transformation.translate(vec);
            this->pCallback->transform(this->id, this->getCalculateAABB3());
        }

        void rotate(float angleInRadians)
        {
            this->transformation.rotate(angleInRadians);
            this->pCallback->transform(this->id, this->getCalculateAABB3());
        }

        void setPosition(common::math::Vector3 position)
        {
            this->transformation.setPosition(position);
            this->pCallback->transform(this->id, this->getCalculateAABB3());
        }

        void setAngle(float angleInRadians)
        {
            this->transformation.setAngle(angleInRadians);
            this->pCallback->transform(this->id, this->getCalculateAABB3());
        }

        RenderableType getType() const
        {
            return this->type;
        }

        int32_t getId() const
        {
            return this->id;
        }

        float getDepth() const
        {
            return this->depth;
        }

        bool operator == (const Renderable &rhs)
        {
            return
                this->id == rhs.id &&
                this->type == rhs.type &&
                this->color == rhs.color &&
                this->transformation == rhs.transformation
           ;
        }

    protected:

        friend class BHVTree;

        int32_t id;
        RenderableType type;

        common::Color4f color;
        ModelTransformation transformation;
        float depth;

        BHVTreeTransformationCallback *pCallback;
    };

}
}