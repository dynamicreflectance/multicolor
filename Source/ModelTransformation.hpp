#pragma once

/* ModelTransformation.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <Matrix4x4.hpp>
#include <Vector2.hpp>
#include <Math.hpp>

namespace dynamicreflectance {
namespace multicolor
{

    class ModelTransformation
    {
    public:

        ModelTransformation()
            : finalTransformationMatrix(common::math::Matrix4x4())
            , rotatationMatrix(common::math::Matrix4x4(1.0f))
            , translationMatrix(common::math::Matrix4x4(1.0f))
            , scaleMatrix(common::math::Matrix4x4(1.0f))
            , positionVector(common::math::Vector4(0.0f, 0.0f, 0.0f, 1.0f))
            , scaleVector(common::math::Vector2(1.0f, 1.0f))
            , angleInRadians(0.0f)
            , modified(true)
        {}

        ModelTransformation(float angleInRadians, common::math::Vector3 position, common::math::Vector2 scale)
            : finalTransformationMatrix(common::math::Matrix4x4(1.0f))
            , rotatationMatrix(common::math::Matrix4x4(1.0f))
            , translationMatrix(common::math::Matrix4x4(1.0f))
            , scaleMatrix(common::math::Matrix4x4(1.0f))
            , positionVector(position.x, position.y, position.z, 1.0f)
            , scaleVector(scale)
            , angleInRadians(angleInRadians)
            , modified(true)
        {}

        ModelTransformation(const ModelTransformation &rhs)
            : finalTransformationMatrix(rhs.finalTransformationMatrix)
            , rotatationMatrix(rhs.rotatationMatrix)
            , translationMatrix(rhs.translationMatrix)
            , scaleMatrix(rhs.scaleMatrix)
            , positionVector(rhs.positionVector)
            , scaleVector(rhs.scaleVector)
            , angleInRadians(rhs.angleInRadians)
            , modified(rhs.modified)
        {}

 
        void translate(common::math::Vector3 vec)
        {
            this->positionVector.xyz += vec;
            this->modified = true;
        }

        void rotate(float angleInRadians)
        {
            this->angleInRadians += angleInRadians;
            this->modified = true;
        }

        void scale(common::math::Vector2 scale)
        {
            this->scaleVector += scale;
            this->modified = true;
        }

        void transform(common::math::Vector2 scale, float angleInRadians, common::math::Vector3 vec)
        {
            this->scaleVector += scale;
            this->angleInRadians += angleInRadians;
            this->positionVector.xyz += vec;
            this->modified = true;
        }

        void setPosition(common::math::Vector3 position)
        {
            this->positionVector = common::math::Vector4(position.x, position.y, position.z, 1.0f);
            this->modified = true;
        }

        void setAngle(float angleInRadians)
        {
            this->angleInRadians = angleInRadians;
            this->modified = true;
        }

        void setScale(common::math::Vector2 scale)
        {
            this->scaleVector = scale;
            this->modified = true;
        }

        void setTransformation(common::math::Vector2 scale, float angleInRadians, common::math::Vector3 position)
        {
            this->scaleVector = scale;
            this->angleInRadians = angleInRadians;
            this->positionVector = common::math::Vector4(position.x, position.y, position.z, 1.0f);
            this->modified = true;
        }

        void reset()
        {
            this->finalTransformationMatrix.setZero();
        }

        common::math::Vector4 getPosition() const
        {
            return this->positionVector;
        }

        common::math::Vector2 getScale() const
        {
            return this->scaleVector;
        }

        float getAngleInRadians() const
        {
            return this->angleInRadians;
        }

        const common::math::Matrix4x4 getCalculateMatrix() const
        {
            if (true == this->modified)
            {
                this->finalTransformationMatrix.setIdentity();

                this->rotatationMatrix.row0.xy = common::math::Vector2(cosf(this->angleInRadians), sinf(this->angleInRadians));
                this->rotatationMatrix.row1.xy = common::math::Vector2(-sinf(this->angleInRadians), cosf(this->angleInRadians));

                this->translationMatrix.row3.xyz = this->positionVector.xyz;

                this->scaleMatrix.row0.x = this->scaleVector.x;
                this->scaleMatrix.row1.y = this->scaleVector.y;

                this->finalTransformationMatrix = this->translationMatrix * this->rotatationMatrix * this->scaleMatrix;

                this->modified = false;
            }

            return this->finalTransformationMatrix;
        }

        bool operator == (const ModelTransformation &rhs) const
        {
            return
                true == common::math::areScalarsEqual(this->angleInRadians, rhs.angleInRadians) &&
                this->scaleVector == rhs.scaleVector &&
                this->positionVector == rhs.positionVector &&
                this->rotatationMatrix == rhs.rotatationMatrix &&
                this->translationMatrix == rhs.translationMatrix &&
                this->scaleMatrix == rhs.scaleMatrix &&
                this->finalTransformationMatrix == rhs.finalTransformationMatrix
                ;
        }

    private:

        mutable common::math::Matrix4x4 finalTransformationMatrix;

        mutable common::math::Matrix4x4 rotatationMatrix;
        mutable common::math::Matrix4x4 translationMatrix;
        mutable common::math::Matrix4x4 scaleMatrix;

        common::math::Vector4 positionVector;
        common::math::Vector2 scaleVector;
        float angleInRadians;

        mutable bool modified;
    };

}
}