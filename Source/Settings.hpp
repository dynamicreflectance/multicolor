#pragma once

/* Settings.hpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include <cstdint>

namespace dynamicreflectance {
namespace settings
{
    constexpr int32_t MULTITEXTURE_TEXTURES_COUNT = 3;
    constexpr int32_t FILE_LOADER_INTERNAL_BUFFER_SIZE_IN_BYTES = 64;
    constexpr float FAT_AABB_FACTOR = 0.3f;
    constexpr char DEFAULT_FOLDER_SEPARATOR = '\\';
}
}
