/* HardwareGeometryBuffer.cpp
*
* Copyright (C) 2016 Dynamic Reflectance
*
* This software may be modified and distributed under the terms
* of the MIT license. See the LICENSE file for details.
*/

#include "HardwareGeometryBuffer.hpp"
#include "OpenGLDriver.hpp"

namespace dynamicreflectance {
namespace multicolor
{
    using namespace common;
    using namespace graphicsdriver;

    HardwareGeometryBuffer::HardwareGeometryBuffer(const uint8_t *pVertices, int32_t verticesSizeInBytes, const uint8_t *pIndices, int32_t indicesSizeInBytes, const std::vector<VertexAttribute> &attributes, BufferUpdateMode updateMode)
        : HardwareGeometryBuffer()
    {
        OpenGLDriver::getInstance().clearErrors();

        GLResource vbo = OpenGLDriver::getInstance().pGeometryBufferUnit->create(BufferType::VBO, updateMode, pVertices, verticesSizeInBytes);
        GLResource ibo = OpenGLDriver::getInstance().pGeometryBufferUnit->create(BufferType::IBO, updateMode, pIndices, indicesSizeInBytes);

        if (DriverError::NO_ERROR == OpenGLDriver::getInstance().getLastError())
        {
            GLResource vao = OpenGLDriver::getInstance().pGeometryBufferUnit->create(vbo, ibo, attributes);

            if (DriverError::NO_ERROR == OpenGLDriver::getInstance().getLastError())
            {
                this->vbo = vbo;
                this->ibo = ibo;
                this->vao = vao;
                this->created = true;
            }
        }

    }

    HardwareGeometryBuffer::~HardwareGeometryBuffer()
    {
        this->decRef();

        if (true == this->isReleased())
        {
            OpenGLDriver::getInstance().pGeometryBufferUnit->release(inout(this->vbo));
            OpenGLDriver::getInstance().pGeometryBufferUnit->release(inout(this->ibo));
            OpenGLDriver::getInstance().pGeometryBufferUnit->release(inout(this->vao));
        }
    }

    bool HardwareGeometryBuffer::set(const uint8_t *pVertices, int32_t verticesSizeInBytes, const uint8_t *pIndices, int32_t indicesSizeInBytes, const std::vector<VertexAttribute> &attributes, BufferUpdateMode updateMode)
    {
        if (true == this->created)
        {
            OpenGLDriver::getInstance().pGeometryBufferUnit->release(inout(this->vbo));
            OpenGLDriver::getInstance().pGeometryBufferUnit->release(inout(this->ibo));
            OpenGLDriver::getInstance().pGeometryBufferUnit->release(inout(this->vao));
        }

        OpenGLDriver::getInstance().clearErrors();

        GLResource vbo = OpenGLDriver::getInstance().pGeometryBufferUnit->create(BufferType::VBO, updateMode, pVertices, verticesSizeInBytes);
        GLResource ibo = OpenGLDriver::getInstance().pGeometryBufferUnit->create(BufferType::IBO, updateMode, pIndices, indicesSizeInBytes);

        if(DriverError::NO_ERROR == OpenGLDriver::getInstance().getLastError())
        {
            GLResource vao = OpenGLDriver::getInstance().pGeometryBufferUnit->create(vbo, ibo, attributes);

            if (DriverError::NO_ERROR == OpenGLDriver::getInstance().getLastError())
            {
                this->vbo = vbo;
                this->ibo = ibo;
                this->vao = vao;
                this->created = true;
            }
            else
            {
                this->created = false;
            }
        }

        return this->created;
    }

    bool HardwareGeometryBuffer::setVbo(const uint8_t *pVertices, int32_t verticesSizeInBytes, BufferUpdateMode updateMode)
    {
        OpenGLDriver::getInstance().clearErrors();
        OpenGLDriver::getInstance().pGeometryBufferUnit->setData(BufferType::VBO, updateMode, this->vbo, pVertices, verticesSizeInBytes);
        return DriverError::NO_ERROR == OpenGLDriver::getInstance().getLastError();
    }

    bool HardwareGeometryBuffer::setIbo(const uint8_t *pIndices, int32_t indicesSizeInBytes, BufferUpdateMode updateMode)
    {
        OpenGLDriver::getInstance().clearErrors();
        OpenGLDriver::getInstance().pGeometryBufferUnit->setData(BufferType::IBO, updateMode, this->ibo, pIndices, indicesSizeInBytes);
        return DriverError::NO_ERROR == OpenGLDriver::getInstance().getLastError();
    }
}
}